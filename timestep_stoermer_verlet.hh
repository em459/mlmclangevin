/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the MLMCLangevin code.
 *  
 *  (c) Copyright Eike Mueller, Grigoris Katsiolides and Tony Shardlow, University of Bath
 *  
 *  Main Developer: Eike Mueller
 *  
 *  Contributors:
 *    Grigoris Katsiolides
 *    Tony Shardlow
 *  
 *  MLMCLangevin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MLMCLangevin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with MLMCLangevin (see files COPYING and COPYING.LESSER). If not,
 *  see <http://www.gnu.org/licenses/>.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/

#ifndef TIMESTEP_STOERMERVERLET_HH
#define TIMESTEP_STOERMERVERLET_HH TIMESTEP_STOERMERVERLET_HH
#include <cmath>
#include <cassert>
#include "timestep_splitting.hh"

/* ************************************* *
 * Class for one time step using
 * the splitting method combining
 * (1) the OU process for random part
 * (2) Stoermer-Verlet for Hamiltonian
 *     system
 * ************************************* */
template <class L>
class TimestepStoermerVerlet : public TimestepSplitting<L,2> {
public:
  typedef TimestepSplitting<L,2> Base;
  static const int nsub = 2;
  /* Constructor */
  TimestepStoermerVerlet(const L& langevin_) :
    Base(langevin_) {}

  /* One time step: first-order splitting: exact OU/symplectic Euler */
  inline void step(const double *xi,
                   double &hi,
                   double &U,
                   double &X,
                   double &S) const {
    OUstep(U,X,0.5*hi,S*xi[0]);
    U -= 0.5*hi*langevin.gradV(X,U);
    X += U*hi;
    langevin.applyBoundaryConditions(X,U,S);
    U -= 0.5*hi*langevin.gradV(X,U);
    OUstep(U,X,0.5*hi,S*xi[1]);

  }

  using Base::langevin;
  using Base::OUstep;
};

#endif // TIMESTEP_STOERMERVERLET_HH
