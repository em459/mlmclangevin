/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the MLMCLangevin code.
 *  
 *  (c) Copyright Eike Mueller, Grigoris Katsiolides and Tony Shardlow, University of Bath
 *  
 *  Main Developer: Eike Mueller
 *  
 *  Contributors:
 *    Grigoris Katsiolides
 *    Tony Shardlow
 *  
 *  MLMCLangevin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MLMCLangevin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with MLMCLangevin (see files COPYING and COPYING.LESSER). If not,
 *  see <http://www.gnu.org/licenses/>.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/

#ifndef STATEDISTRIBUTION_HH
#define STATEDISTRIBUTION_HH STATEDISTRIBUTION_HH

#include <cstdlib>
#include "distribution.hh"

/* ************************************* *
 * Base class for probability distribution
 * for U and X
 * ************************************* */
class StateDistribution {
public:
  StateDistribution(const ParametersDistribution& parameters_dist_) :
    parameters_dist(parameters_dist_) {
    srand(parameters_dist.Seed());
  }
  inline void draw(double &U, double &X) const {}
protected:
  const ParametersDistribution& parameters_dist;
};

#endif // STATEDISTRIBUTION_HH
