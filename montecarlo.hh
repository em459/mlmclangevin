/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the MLMCLangevin code.
 *  
 *  (c) Copyright Eike Mueller, Grigoris Katsiolides and Tony Shardlow, University of Bath
 *  
 *  Main Developer: Eike Mueller
 *  
 *  Contributors:
 *    Grigoris Katsiolides
 *    Tony Shardlow
 *  
 *  MLMCLangevin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MLMCLangevin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with MLMCLangevin (see files COPYING and COPYING.LESSER). If not,
 *  see <http://www.gnu.org/licenses/>.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/

#ifndef MONTECARLO_HH           //
#define MONTECARLO_HH MONTECARLO_HH
#include <ctime>
#include <algorithm>
#include <string>
#include "langevin.hh"
#include "config.h"
#include "distribution.hh"
#include "quantityofinterest.hh"
#include "parameters.hh"
#include "messages.hh"
#include "timer.hh"

/* ************************************* *
 * Parameters for Monte Carlo class
 * ************************************* */
class ParametersMonteCarlo : public Parameters {
public:
  // Constructor
  ParametersMonteCarlo() :
    Parameters("montecarlo"),
    T_(1.0),
    L_(7),
    Mcoarse_(1),
    M_(Mcoarse_ << L_),
    h_(T_/M_),
    epsilon_(1.e-2),
    adaptive_(false) {
    addKey("T",Double);
    addKey("epsilon",Double);
    addKey("L",Integer);
    addKey("Mcoarse",Integer);
    addKey("adaptive",Bool);
  }

  // Read from file
  int readFile(const std::string filename) {

    int readSuccess = Parameters::readFile(filename);
    if (!readSuccess) {
      T_ = contents["T"]->getDouble();
      epsilon_ = contents["epsilon"]->getDouble();
      // Finest multigrid level
      L_ = contents["L"]->getInt();
      Mcoarse_ = contents["Mcoarse"]->getInt();
      adaptive_ = contents["adaptive"]->getBool();
      M_= (Mcoarse_ << L_);
      h_ = T_/double(M_);
    }
    return readSuccess;
  }

  // Return data
  unsigned int Mcoarse() const { return Mcoarse_; }
  unsigned int L() const { return L_; }
  double T() const { return T_; }
  unsigned int M() const { return M_; }
  double h() const { return h_; }
  double epsilon() const { return epsilon_; }
  bool adaptive() const { return adaptive_; }

  // Class data
private:
  double T_;
  double epsilon_;
  unsigned int L_;
  unsigned int Mcoarse_;
  unsigned int M_;
  double h_;
  bool adaptive_;
};

/* ************************************* *
 * Virtual Base class for Monte Carlo integrator
 * ************************************* */
template <class Distribution,
          class Timestep,
          class QuantityOfInterest,
          class InitialDistribution>
class MonteCarlo {
public:
  MonteCarlo(const Distribution &distribution_,
             Timestep &timestep_,
             const QuantityOfInterest &qoi_,
             const InitialDistribution &initialdistribution_,
             const ParametersMonteCarlo &parameters_montecarlo_,
             const bool verbose_=false) :
    distribution(distribution_),
    timestep(timestep_),
    qoi(qoi_),
    initialdistribution(initialdistribution_),
    verbose(verbose_),
    parameters_montecarlo(parameters_montecarlo_),
    messagehandler() {}

  // Run
  virtual void run() = 0;

  /* Return expectation value of quantity of interest */
  virtual std::vector<double> E_QoI() const = 0;

  /* Return error of QoI */
  virtual std::vector<double> Error_QoI() const = 0;

  /* Return cost */
  virtual double Cost() const { return cost; }

  /* print out results */
  virtual void show_results() const = 0;

protected:
  const Distribution &distribution;
  Timestep &timestep;
  const QuantityOfInterest &qoi;
  const InitialDistribution &initialdistribution;
  const ParametersMonteCarlo &parameters_montecarlo;
  bool verbose;
  double cost;
  const MessageHandler messagehandler;
  Timer t_run;
};
#endif // MONTECARLO_HH
