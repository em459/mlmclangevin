/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the MLMCLangevin code.
 *  
 *  (c) Copyright Eike Mueller, Grigoris Katsiolides and Tony Shardlow, University of Bath
 *  
 *  Main Developer: Eike Mueller
 *  
 *  Contributors:
 *    Grigoris Katsiolides
 *    Tony Shardlow
 *  
 *  MLMCLangevin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MLMCLangevin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with MLMCLangevin (see files COPYING and COPYING.LESSER). If not,
 *  see <http://www.gnu.org/licenses/>.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/

#ifndef TIMESTEP_BAOAB_HH
#define TIMESTEP_BAOAB_HH TIMESTEP_BAOAB_HH
#include <cmath>
#include <cassert>
#include "timestep_splitting.hh"

/* ************************************* *
 * Class for one time step using the
 * Symmetric Langevin Velocity-Verlet
 * method as in the book Molecular
 * Dynamics
 * ************************************* */
template <class L>
class TimestepBAOAB : public TimestepSplitting<L,1> {
public:
  typedef TimestepSplitting<L,1> Base;
  /* Constructor */
  TimestepBAOAB<L>(const L& langevin_) :
  Base(langevin_) {}

  // Combine increments
  inline void CombineIncrements(const double *I_fine,
                                double *I_coarse,
                                double &hi,
                                const double X) const {
    double mu=exp(-hi*langevin.lambda(X));
    I_coarse[0] = (mu*I_fine[0]+I_fine[1]) / sqrt(mu*mu+1.0);
  }

  /* One time step */
  inline void step(const double *xi,
                   double &hi,
                   double &U,
                   double &X,
                   double &S) const {
    U -= langevin.gradV(X,U)*0.5*hi;
    X += U*0.5*hi;
    langevin.applyBoundaryConditions(X,U,S);
    // OU step
    OUstep(U,X,hi,S*xi[0]);
    X += U*0.5*hi;
    langevin.applyBoundaryConditions(X,U,S);
    U -= langevin.gradV(X,U)*0.5*hi;
  }

  using Base::langevin;
  using Base::OUstep;
};

#endif // TIMESTEP_BAOAB_HH
