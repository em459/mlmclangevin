/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the MLMCLangevin code.
 *  
 *  (c) Copyright Eike Mueller, Grigoris Katsiolides and Tony Shardlow, University of Bath
 *  
 *  Main Developer: Eike Mueller
 *  
 *  Contributors:
 *    Grigoris Katsiolides
 *    Tony Shardlow
 *  
 *  MLMCLangevin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MLMCLangevin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with MLMCLangevin (see files COPYING and COPYING.LESSER). If not,
 *  see <http://www.gnu.org/licenses/>.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/

#ifndef DISTRIBUTIONBIASMC_HH           //
#define DISTRIBUTIONBIASMC_HH DISTRIBUTIONBIASMC_HH
#include <ctime>
#include <algorithm>
#include "montecarlo.hh"
#include "langevin.hh"
#include "distribution.hh"
#include "quantityofinterest.hh"
#include "estimator.hh"
#include "timer.hh"

/* ************************************* *
 * Monte Carlo integrator for measuring distribution bias
 * ************************************* */
template <class Distribution,
          class Timestep,
          class QuantityOfInterest,
          class InitialDistribution>
class MonteCarloDistributionBias : public MonteCarlo<Distribution,Timestep,QuantityOfInterest,InitialDistribution>{
public:
  typedef MonteCarlo<Distribution,Timestep,QuantityOfInterest,InitialDistribution> Base;
  MonteCarloDistributionBias(const Distribution &distribution_,
                             Timestep &timestep_,
                             const QuantityOfInterest &qoi_,
                             const InitialDistribution &initialdistribution_,
                             const ParametersMonteCarlo &parameters_montecarlo_,
                             const bool verbose_=false) :
    Base(distribution_,
         timestep_,
         qoi_,
         initialdistribution_,
         parameters_montecarlo_,
         verbose_), N(0), timestep_fine(timestep_),
    estimator(qoi_.N()) {
  }

  // Run the integrator
  virtual void run() {
    t_run.reset();
    t_run.start();
    unsigned int M = parameters_montecarlo.M();
    double epsilon = parameters_montecarlo.epsilon();
    double h_uniform = parameters_montecarlo.h();
    double T = parameters_montecarlo.T();
    double time = T - 1.e-12; // makes sure that the while loop will not exceed T due to rounding errors
    double X, X_combined;
    double U, U_combined;
    estimator.reset();
    // Initial guess for number of samples
    N = 1000;
    bool sufficientStat = false;
    do {
      // Loop over particles
      unsigned int N_0 = estimator.samples();
      for (unsigned int i=N_0;i<N;++i) {
        // Loop over timesteps
        initialdistribution.draw(U,X);
        initialdistribution.draw(U_combined,X_combined);
        double t = 0.0;
        double S_distributionbias = 1.0;
        while (t < time) {
          double xi_fine[2*Timestep::nxi];
          double xi[Timestep::nxi];
          distribution.draw(2*Timestep::nxi,xi_fine);
          // Combine increments and step with them
          double h_uniform_fine = 0.5*h_uniform;
          timestep_fine.CombineIncrements(xi_fine,xi,h_uniform_fine,X_combined);
          timestep.step(xi,h_uniform,U_combined,X_combined,S_distributionbias);
          // Step using uncombined increments
          timestep.step(xi_fine,h_uniform,U,X,S_distributionbias);
          t+=h_uniform;
        }
        std::vector<double> diff(qoi.evaluate(X,U).size());
        for (size_t i=0; i<diff.size(); ++i) {
          diff[i] = qoi.evaluate(X,U)[i]-qoi.evaluate(X_combined,U_combined)[i];
        }
        estimator.accumulate(diff);
      }
      N = (unsigned int) ceil(2./(epsilon*epsilon)*estimator.max_variance());
      if (verbose) {
        std::cout << "   ...adjusting N to " << N << std::endl;
      }
      sufficientStat = (estimator.samples() >= N);
    } while (!sufficientStat);
    t_run.stop();
  }

  /* Return expectation value of quantity of interest */
  virtual std::vector<double> E_QoI() const { return estimator.mean(); }

  /* Return variance of quantity of interest */
  virtual std::vector<double> Var_QoI() const { return estimator.variance(); }

  /* Return error of QoI */
  virtual std::vector<double> Error_QoI() const {
    std::vector<double> error = Var_QoI();
    for (size_t i=0; i<error.size(); ++i) {
      error[i] = std::sqrt(error[i]/N);
    }
    return error;
  }

  /* Return number of particles */
  virtual unsigned int Npart() const { return N; }

  /* print out results */
  virtual void show_results() const {
    std::cout.precision(5);
    std::cout << " h = " << std::scientific << parameters_montecarlo.h() << std::endl;
    std::cout << " Number of part.      = " << N << std::endl;
    std::cout.precision(10);
    for (size_t i=0; i<E_QoI().size(); ++i) {
      std::cout << " Expectation value" << "[" << i+1 << "] = " << std::fixed << E_QoI()[i];
      std::cout << " +/- " << Error_QoI()[i];
      std::cout << std::endl;
    }
    for (size_t i=0; i<Var_QoI().size(); ++i) {
      std::cout << " Variance" << "[" << i+1 << "]          = " << Var_QoI()[i] << std::endl;
    }
    std::cout << std::endl;
    std::cout.precision(5);
    std::cout << " Elapsed time = " << std::scientific << t_run.elapsed() << " s" << std::endl;
    std::cout << std::endl;
  }

private:
  using Base::distribution;
  using Base::timestep;
  using Base::qoi;
  using Base::initialdistribution;
  using Base::parameters_montecarlo;
  using Base::verbose;
  using Base::messagehandler;
  using Base::t_run;
  Estimator estimator;
  unsigned int N;
  Timestep timestep_fine;
  double X_tmp;
};
#endif // DISTRIBUTIONBIASMC_HH
