/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the MLMCLangevin code.
 *  
 *  (c) Copyright Eike Mueller, Grigoris Katsiolides and Tony Shardlow, University of Bath
 *  
 *  Main Developer: Eike Mueller
 *  
 *  Contributors:
 *    Grigoris Katsiolides
 *    Tony Shardlow
 *  
 *  MLMCLangevin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MLMCLangevin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with MLMCLangevin (see files COPYING and COPYING.LESSER). If not,
 *  see <http://www.gnu.org/licenses/>.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/

#ifndef TIMER_HH                //
#define TIMER_HH TIMER_HH

#include <time.h>
#include <string>
#include <iostream>
#ifdef USE_CHRONO
#include <chrono>
#endif // USE_CHRONO

/* ************************************* *
 * Class for timer
 * ************************************* */
class Timer {
public:
#ifdef USE_CHRONO
  typedef std::chrono::high_resolution_clock Clock;
  typedef std::chrono::time_point<Clock> Timepoint;
#else
  typedef clock_t Timepoint;
#endif // USE_CHRONO
  Timer(const std::string label_="") :
    label(label_),
    t_elapsed(0.0) {}
  // Return elapsed time in seconds
  double elapsed() const { return t_elapsed; }
  // Reset timer
  void reset();
  // Start timer again
  void start();
  // Stop timer
  void stop();
  // get label
  std::string Label() const { return label; }
  // Resolution
  static double resolution();
  // timer type
  static const std::string type;
private:
  std::string label;
  double t_elapsed;
  Timepoint tp_start;
  Timepoint tp_finish;
};

// Write timer to stream
std::ostream& operator<<(std::ostream& out, Timer& t);

#endif // TIMER_HH
