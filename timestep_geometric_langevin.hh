/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the MLMCLangevin code.
 *  
 *  (c) Copyright Eike Mueller, Grigoris Katsiolides and Tony Shardlow, University of Bath
 *  
 *  Main Developer: Eike Mueller
 *  
 *  Contributors:
 *    Grigoris Katsiolides
 *    Tony Shardlow
 *  
 *  MLMCLangevin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MLMCLangevin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with MLMCLangevin (see files COPYING and COPYING.LESSER). If not,
 *  see <http://www.gnu.org/licenses/>.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/

#ifndef TIMESTEP_GEOMETRICLANGEVIN_HH
#define TIMESTEP_GEOMETRICLANGEVIN_HH TIMESTEP_GEOMETRICLANGEVIN_HH
#include <cmath>
#include <cassert>
#include "timestep_splitting.hh"

/* ************************************* *
 * Class for one time step using
 * the splitting method combining
 * (1) the OU process for random part
 * (2) symplectic Euler for Hamiltonian
 *     system
 * ************************************* */
/* This method was used in the paper
 * 'Improving MLMC for SDEs with
 * application to the Langevin equation'
 * with the name 'Symplectic Euler/OU'
 * but we then renamed it to 'Geometric
 * Langevin' 
 * ************************************* */
template <class L>
class TimestepGeometricLangevin : public TimestepSplitting<L,1> {
public:
  typedef TimestepSplitting<L,1> Base;
  /* Constructor */
  TimestepGeometricLangevin<L>(const L& langevin_) :
  Base(langevin_) {}

  /* One time step: first-order splitting: exact OU/symplectic Euler */
  inline void step(const double *xi,
                   double &hi,
                   double &U,
                   double &X,
                   double &S) const {
    // OU step
    OUstep(U,X,hi,S*xi[0]);
    // Symplectic Euler step
    U -= langevin.gradV(X,U)*hi;
    X += U*hi;
    langevin.applyBoundaryConditions(X,U,S);
  }

  /* One time adaptive step: input random variables already have their 
     variance equal to the timestep size */
  inline void AdaptiveStep(const double *xi,
                           double &hi,
                           double &U,
                           double &X,
                           double &S) const {
    // OU step
    OUstepAdaptive(U,X,hi,S*xi[0]);
    // Symplectic Euler step
    U -= langevin.gradV(X,U)*hi;
    X += U*hi;
    langevin.applyBoundaryConditions(X,U,S);
  }

  //using Base::h;
  using Base::langevin;
  using Base::OUstep;
  using Base::OUstepAdaptive;
};

#endif // TIMESTEP_GEOMETRICLANGEVIN_HH
