/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the MLMCLangevin code.
 *  
 *  (c) Copyright Eike Mueller, Grigoris Katsiolides and Tony Shardlow, University of Bath
 *  
 *  Main Developer: Eike Mueller
 *  
 *  Contributors:
 *    Grigoris Katsiolides
 *    Tony Shardlow
 *  
 *  MLMCLangevin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MLMCLangevin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with MLMCLangevin (see files COPYING and COPYING.LESSER). If not,
 *  see <http://www.gnu.org/licenses/>.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/

#ifndef MESSAGES_HH
#define MESSAGES_HH MESSAGES_HH

#include <iostream>
#include <string>

/* ************************************* *
 * Class for handling messages
 * ************************************* */
class MessageHandler {
public:
  // Constructor
  MessageHandler() {}

  // Fatal error
  void fatalError(const std::string msg,
                  const std::string methodName="",
                  const int errorcode=-1) const {
    std::cerr << " >>> FATAL ERROR";
    if (methodName != "") {
      std::cerr << " in method " << methodName;
    }
    std::cerr << " : ";
    std::cerr << msg << std::endl;
    exit(errorcode);
  }

  // Non-fatal error
  void error(const std::string msg,
             const std::string methodName="",
             const int errorcode=-1) const {
    std::cerr << " >>> ERROR";
    if (methodName != "") {
      std::cerr << " in method " << methodName;
    }
    std::cerr << " : ";
    std::cerr << msg << std::endl;
  }

  // Warning
  void warning(const std::string msg,
               const std::string methodName="") const {
    std::cout << " >>> WARNING";
    if (methodName != "") {
      std::cout << " in method " << methodName;
    }
    std::cout << " : ";
    std::cout << msg << std::endl;
  }

  // Information
  void info(const std::string msg) const {
    std::cout << " >>> INFORMATION " << msg << std::endl;
  }
private:
};

#endif // MESSAGES_HH
