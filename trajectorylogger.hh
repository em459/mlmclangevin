/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the MLMCLangevin code.
 *  
 *  (c) Copyright Eike Mueller, Grigoris Katsiolides and Tony Shardlow, University of Bath
 *  
 *  Main Developer: Eike Mueller
 *  
 *  Contributors:
 *    Grigoris Katsiolides
 *    Tony Shardlow
 *  
 *  MLMCLangevin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MLMCLangevin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with MLMCLangevin (see files COPYING and COPYING.LESSER). If not,
 *  see <http://www.gnu.org/licenses/>.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/

#ifndef TRAJECTORYLOGGER_HH
#define TRAJECTORYLOGGER_HH TRAJECTORYLOGGER_HH

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <exception>

// Dummy class, does nothing
class TrajectoryLoggerDummy {
public:
  // Constructor
  TrajectoryLoggerDummy() {}
  // Destructor
  ~TrajectoryLoggerDummy() {}
  void open() {}
  void close() {}
  void start_trajectory() {}
  void finish_trajectory() {}
  inline void add(const double t, const double X, const double U) {}
};

// Log trajectories in a file
class TrajectoryLoggerFile {
public:
  typedef std::vector<double> PointType;
  // Constructor
  TrajectoryLoggerFile(const std::string filename_,
                       const int max_trajectories_=100) : 
    filename(filename_), 
    max_trajectories(max_trajectories_) {}
  // Destructor
  ~TrajectoryLoggerFile() {}
  // Open file for recording trajectories
  void open() {
    try {
      datafile.open(filename.c_str(),std::ios::out);
    } catch (std::exception &e) {
      std::cout << "ERROR opening trajectory file \'" << filename << "\'";
      std::cout << " for writing : " << e.what() << std::endl;
      exit(-1);
    }
    current_position = 0;
    current_trajectory = 0;
    datafile << "#### Trajectory file ####" << std::endl;
    datafile << "# column 1: time" << std::endl;
    datafile << "# column 2: position" << std::endl;
    datafile << "# column 3: velocity" << std::endl;
    datafile << std::endl;
  }
  // Close file for recording trajectories
  void close() {
    try {
      datafile.close();
    } catch (std::exception &e) {
      std::cout << "ERROR closing trajectory file \'" << filename << "\'";
      std::cout << " : " << e.what() << std::endl;
      exit(-1);
    }
    current_position = 0;
    current_trajectory = 0;
  }
  // Start new trajectory
  inline void start_trajectory() {
    current_position = 0;
  }
  // Save trajectory to file
  inline void finish_trajectory() {    
    if (current_trajectory < max_trajectories) {
      std::vector<PointType>::const_iterator ibegin = trajectory.begin();
      std::vector<PointType>::const_iterator iend = trajectory.end();
      datafile.precision(8);
      datafile.width(12);
      datafile << "# trajectory " << current_trajectory << std::endl;
      for (std::vector<PointType>::const_iterator it=ibegin; it!=iend; ++it) {
        datafile << (*it)[0] << " " << (*it)[1] << " " << (*it)[2] << std::scientific;
        datafile << std::endl;
      }
      datafile << std::endl;
      current_position = 0;
      current_trajectory++;
    }
  }
  // Add point to current trajectory
  inline void add(const double t, const double X, const double U) {
    if (current_trajectory < max_trajectories) {
      PointType p(3);
      p[0] = t; p[1] = X; p[2] = U;
      if (current_position < trajectory.size()) {
        trajectory[current_position] = p;
      } else {
        trajectory.push_back(p);
      }
      current_position++;
    }
  }
private:
  const std::string filename; // Name of file to write to
  const int max_trajectories; // Maximal number of recorded trajectories
  int current_position; // Current position in trajectory
  int current_trajectory; // Current trajectory number 
  std::ofstream datafile; // output stream to write to
  std::vector<PointType> trajectory; // trajectory data
};

#endif // TRAJECTORYLOGGER_HH
