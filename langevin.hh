/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the MLMCLangevin code.
 *  
 *  (c) Copyright Eike Mueller, Grigoris Katsiolides and Tony Shardlow, University of Bath
 *  
 *  Main Developer: Eike Mueller
 *  
 *  Contributors:
 *    Grigoris Katsiolides
 *    Tony Shardlow
 *  
 *  MLMCLangevin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MLMCLangevin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with MLMCLangevin (see files COPYING and COPYING.LESSER). If not,
 *  see <http://www.gnu.org/licenses/>.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/

#ifndef LANGEVIN_HH
#define LANGEVIN_HH LANGEVIN_HH

#include <cstdlib>
#include <cmath>
#include "statedistribution.hh"

/* **************************************** *
   Base class representing a Langevin process
   * **************************************** */

class LangevinBase : public StateDistribution {
public:
  typedef StateDistribution Base;
  // Constructor
  LangevinBase(const ParametersDistribution& parameters_dist_) :
    Base(parameters_dist_) {}
  // lambda(X)
  inline double lambda(const double X) const { return 1.0; };
  // sigma(X)
  inline double sigma(const double X) const { return 1.0; };
  // Value of potential
  inline double V(const double X, const double U) const {return 0.0; };
  // Gradient of potential
  inline double gradV(const double X, const double U) const { return 0.0; };
  // Adapt time step size
  inline double adaptTimestep(const double h_uniform,
                              const double &X) const {
    messagehandler.fatalError("Adaptivity not implemented for this Langevin process");
    return 0.0;
  }
  // Apply boundary conditions
  inline void applyBoundaryConditions(double& X,
                                      double& U,
                                      double& S) const {
  }

private:
  const MessageHandler messagehandler;
};

#endif // LANGEVIN_HH
