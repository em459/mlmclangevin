### COPYRIGHT AND LICENSE STATEMENT ###
#
#  This file is part of the MLMCLangevin code.
#  
#  (c) Copyright Eike Mueller, Grigoris Katsiolides and Tony Shardlow,
#      University of Bath
#  
#  Main Developer: Eike Mueller
#  
#  Contributors:
#    Grigoris Katsiolides
#    Tony Shardlow
#  
#  MLMCLangevin is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#  
#  MLMCLangevin is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.
#  
#  You should have received a copy of the GNU Lesser General Public License
#  along with MLMCLangevin (see files COPYING and COPYING.LESSER). If not,
#  see <http://www.gnu.org/licenses/>.
#
### COPYRIGHT AND LICENSE STATEMENT ###

#################################################################
# User specified options
#################################################################

# Use chrono library for timing?
# (requires compiler which supports at c++-11 standard)
USE_CHRONO=true

# Use library version of random number generators?
# (requires compiler which supports at least c++-0x standard)
USE_GNU_RNG=true

# Compile mode (Debug or Optimised)
#MODE=Debug
MODE=Optimised

# Record trajectories in standard MC?
RECORD_TRAJECTORIES=false
MAX_TRAJECTORIES=200

#################################################################

# Compiler
CC=g++
CFLAGS=
LFLAGS=

ifeq ($(MODE),Debug)
	CFLAGS := $(CFLAGS) -O0 -g -Wfatal-errors
endif

ifeq ($(MODE),Optimised)
	CFLAGS := $(CFLAGS) -O3 -DNDEBUG -ffast-math
endif

OBJS=parameters.o timer.o
MAINOBJ= driver.o
TESTVECTOROBJ=test_vector.o

# Use the chrono library for timing?
ifeq ($(USE_CHRONO),true)
  CFLAGS := $(CFLAGS) -DUSE_CHRONO
  CXXSTD=c++11
endif

# Use the gnu library random number generator?
ifeq ($(USE_GNU_RNG),true)
  CFLAGS := $(CFLAGS) -DUSE_GNU_RNG
  ifeq ($(CXXSTD),)
    CXXSTD=c++0x
  endif
endif

# Choose c++ standard
ifneq ($(CXXSTD),)
  CFLAGS := $(CFLAGS) -std=$(CXXSTD)
endif

# Record trajectories in a file?
ifeq ($(RECORD_TRAJECTORIES),true)
	CFLAGS := $(CFLAGS) -DRECORD_TRAJECTORIES -DMAX_TRAJECTORIES=$(MAX_TRAJECTORIES)
endif

# Main executable
MAIN=driver.x
TESTVECTOR=test_vector.x

.phony: all
all: $(MAIN)  $(TESTVECTOR)

# Pattern rules for compilation
%.o: %.cc %.hh
	$(CC) $(CFLAGS) -c -o $@ $<

%.o: %.cc
	$(CC) $(CFLAGS) -c -o $@ $<

# Dependencies
driver.o: \
	parameters.o \
  timer.o

driver.o: \
	config.h \
	timer.hh \
  langevin.hh \
	langevin_ho.hh \
	langevin_hdm.hh \
	langevin_idm.hh \
	distribution.hh \
	distribution_discrete.hh \
	distribution_normal.hh \
	distribution_twopoint.hh \
	distribution_threepoint.hh \
	distribution_fourpoint.hh \
	timestep_splitting.hh\
	timestep_base.hh\
	timestep_geometric_langevin.hh \
	timestep_stoermer_verlet.hh \
	timestep_euler.hh \
	timestep_bivariate_ou.hh \
	timestep_ou.hh \
	timestep_baoab.hh \
	statedistribution.hh \
	statedistribution_fixed.hh \
	quantityofinterest.hh \
	estimator.hh \
	messages.hh \
	trajectorylogger.hh \
	montecarlo.hh \
	montecarlo_distributionbias.hh \
	montecarlo_standard.hh \
	montecarlo_multilevel.hh

test_vector.o: \
  vector.hh \
  diagonal_matrix.hh

# Link
$(MAIN): $(OBJS) $(MAINOBJ)
	$(CC) $(LFLAGS) -o $(MAIN) $(OBJS) $(MAINOBJ)

$(TESTVECTOR): $(TESTVECTOROBJ)
	$(CC) $(LFLAGS) -o $(TESTVECTOR) $(TESTVECTOROBJ)

# Tidy up
.phony: clean
clean:
	rm -rf *.o *.x
