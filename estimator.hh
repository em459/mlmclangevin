/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the MLMCLangevin code.
 *  
 *  (c) Copyright Eike Mueller, Grigoris Katsiolides and Tony Shardlow, University of Bath
 *  
 *  Main Developer: Eike Mueller
 *  
 *  Contributors:
 *    Grigoris Katsiolides
 *    Tony Shardlow
 *  
 *  MLMCLangevin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MLMCLangevin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with MLMCLangevin (see files COPYING and COPYING.LESSER). If not,
 *  see <http://www.gnu.org/licenses/>.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/

#ifndef ESTIMATOR_HH
#define ESTIMATOR_HH ESTIMATOR_HH

#include <cassert>
#include <vector>

/* ************************************* *
 * Class for statistical estimator
 * ************************************* */
class Estimator {
public:
  /* ************************************* *
   * Constructor
   * ************************************* */
  Estimator(size_t n_, const bool isexact_=false) :
    n(n_), isexact(isexact_), Nsample(0), Q_sum(n_,0.0), QQ_sum(n_,0.0) {}

  /* ************************************* *
   * Reset estimator
   * ************************************* */
  void reset() {
    Nsample = 0;
    std::fill(Q_sum.begin(),Q_sum.end(),0.0);
    std::fill(QQ_sum.begin(),QQ_sum.end(),0.0);
  }

  /* ************************************* *
   * Add new value
   * ************************************* */
  inline void accumulate(const std::vector<double> q) {
    assert(!isexact);
    for (size_t i=0; i<Q_sum.size(); ++i) {
      double q_i = q[i];
      Q_sum[i] += q_i;
      QQ_sum[i] += q_i*q_i;
    }
    Nsample++;
  }

  /* ************************************* *
   * Add new value with probability
   * (for exact estimator)
   * ************************************* */
  inline void accumulate(const std::vector<double> q, const double p) {
    assert(isexact);
    for (size_t i=0; i<Q_sum.size(); ++i) {
      double q_i = q[i];
      Q_sum[i] += q_i*p;
      QQ_sum[i] += q_i*q_i*p;
    }   
  }

  /* ************************************* *
   * Number of samples
   * ************************************* */
  unsigned int samples() const { return Nsample; }

  /* ************************************* *
   * Calculate mean
   * ************************************* */
  const std::vector<double> mean() const {
    std::vector<double> mean_vec(n,0.0);
    if (isexact) {
      return Q_sum;
    } else {
      if (Nsample > 0) {
        const double inv_Nsample=1.0/Nsample;
        for (size_t i=0; i<n; ++i) {
          mean_vec[i] = Q_sum[i]*inv_Nsample;
        }
        return mean_vec;
      }
    }
    return mean_vec;
  }

  /* ************************************* *
   * Calculate variance
   * ************************************* */
  const std::vector<double> variance() const {
    std::vector<double> var(n,0.0);
    if (isexact) {
      for (size_t i=0; i<Q_sum.size(); ++i) {
        double qsum_i = Q_sum[i];
        double qqsum_i = QQ_sum[i];
        var[i] = qqsum_i - qsum_i*qsum_i;
      }
    } else {
      if (Nsample > 0) {
        const double inv_Nsample=1.0/Nsample;
        const double inv_NsampleMinus=1.0/(Nsample - 1.0);
        for (size_t i=0; i<Q_sum.size(); ++i) {
          double qsum_i = Q_sum[i];
          double qqsum_i = QQ_sum[i];
          var[i] = inv_NsampleMinus*(qqsum_i - qsum_i*qsum_i*inv_Nsample);
        }
      }
    }
    return var;
  }

  double max_variance() const {
    std::vector<double> var = variance();
    std::vector<double>::iterator it=std::max_element(var.begin(),var.end());
    return *it;
  } 

private:
  bool isexact;   // Exact estimator?
  std::vector<double> Q_sum;   // Sum_{i=1,...,N} q_i
  std::vector<double> QQ_sum;  // Sum_{i=1,...,N} q_i^2
  size_t n;
  unsigned int Nsample; // Number of samples
};

#endif // ESTIMATOR_HH
