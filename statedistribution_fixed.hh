/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the MLMCLangevin code.
 *  
 *  (c) Copyright Eike Mueller, Grigoris Katsiolides and Tony Shardlow, University of Bath
 *  
 *  Main Developer: Eike Mueller
 *  
 *  Contributors:
 *    Grigoris Katsiolides
 *    Tony Shardlow
 *  
 *  MLMCLangevin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MLMCLangevin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with MLMCLangevin (see files COPYING and COPYING.LESSER). If not,
 *  see <http://www.gnu.org/licenses/>.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/

#ifndef STATEDISTRIBUTION_FIXED_HH
#define STATEDISTRIBUTION_FIXED_HH STATEDISTRIBUTION_FIXED_HH

#include <cstdlib>

/* ************************************* *
 * Parameters for Initial State distribution
 * ************************************* */
class ParametersInitialDistributionFixed : public Parameters {
public:
  // Constructor
  ParametersInitialDistributionFixed() :
    Parameters("initialdistributionfixed"),
    U0_(0.0),
    X0_(0.0) {
    addKey("U0",Double);
    addKey("X0",Double);
  }

  // Read from file
  int readFile(const std::string filename) {
    int readSuccess = Parameters::readFile(filename);
    if (!readSuccess) {
      U0_ = contents["U0"]->getDouble();
      X0_ = contents["X0"]->getDouble();
    }
    return readSuccess;
  }

  // Return data
  double U0() const { return U0_; }
  double X0() const { return X0_; }

  // Class data
private:
  double U0_;
  double X0_;
};

/* ************************************* *
 * Base class for probability distribution
 * for U and X
 * ************************************* */
class StateDistributionFixed : public StateDistribution {
public:
  typedef StateDistribution Base;
  StateDistributionFixed(const ParametersInitialDistributionFixed& parameters_initialdistributionfixed_, const ParametersDistribution& parameters_dist_) :
    Base(parameters_dist_),
    parameters_initialdistributionfixed(parameters_initialdistributionfixed_),
    U0(parameters_initialdistributionfixed.U0()),
    X0(parameters_initialdistributionfixed.X0()) {}
  inline void draw(double &U, double &X) const {
    U = U0;
    X = X0;
  }
private:
  const ParametersInitialDistributionFixed& parameters_initialdistributionfixed;
  const double U0;
  const double X0;
};

#endif // STATEDISTRIBUTION_FIXED_HH
