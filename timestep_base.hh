/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the MLMCLangevin code.
 *  
 *  (c) Copyright Eike Mueller, Grigoris Katsiolides and Tony Shardlow, University of Bath
 *  
 *  Main Developer: Eike Mueller
 *  
 *  Contributors:
 *    Grigoris Katsiolides
 *    Tony Shardlow
 *  
 *  MLMCLangevin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MLMCLangevin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with MLMCLangevin (see files COPYING and COPYING.LESSER). If not,
 *  see <http://www.gnu.org/licenses/>.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/

#ifndef TIMESTEP_BASE_HH
#define TIMESTEP_BASE_HH TIMESTEP_BASE_HH
#include <cmath>
#include <string>
#include <iostream>
#include "parameters.hh"
#include "messages.hh"

/* ************************************* *
 * General timestep base class
 * ************************************* */
template <class L>
class TimestepBase{
public:
  /* Constructor */
  TimestepBase(const L& langevin_) :
    langevin(langevin_) {}

public:
  inline void AdaptiveStep(const double *xi,
                           double &hi,
                           double &U,
                           double &X,
                           double &S) const {
    messagehandler.fatalError("Adaptive timestepping not implemented for the Multilevel Monte Carlo with this timestep method.");
  }

  // Adapt timestep size
  inline double adapt(const double h_uniform,
                      const double &X) const {
    return langevin.adaptTimestep(h_uniform,X);
  }

protected:
  const L& langevin;
  const MessageHandler messagehandler;
};

#endif // TIMESTEP_BASE_HH
