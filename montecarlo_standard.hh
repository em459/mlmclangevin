/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the MLMCLangevin code.
 *  
 *  (c) Copyright Eike Mueller, Grigoris Katsiolides and Tony Shardlow, University of Bath
 *  
 *  Main Developer: Eike Mueller
 *  
 *  Contributors:
 *    Grigoris Katsiolides
 *    Tony Shardlow
 *  
 *  MLMCLangevin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MLMCLangevin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with MLMCLangevin (see files COPYING and COPYING.LESSER). If not,
 *  see <http://www.gnu.org/licenses/>.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/

#ifndef STANDARDMC_HH           //
#define STANDARDMC_HH STANDARDMC_HH
#include <ctime>
#include <algorithm>
#include <vector>
#include "montecarlo.hh"
#include "langevin.hh"
#include "distribution.hh"
#include "quantityofinterest.hh"
#include "estimator.hh"
#include "timer.hh"
#include "trajectorylogger.hh"

/* ************************************* *
 * Standard Monte Carlo integrator
 * ************************************* */
template <class Distribution,
          class Timestep,
          class QuantityOfInterest,
          class InitialDistribution,
          class TrajectoryLogger>
class MonteCarloStandard : public MonteCarlo<Distribution,Timestep,QuantityOfInterest,InitialDistribution>{
public:
  typedef MonteCarlo<Distribution,Timestep,QuantityOfInterest,InitialDistribution> Base;
  MonteCarloStandard(const Distribution &distribution_,
                     Timestep &timestep_,
                     const QuantityOfInterest &qoi_,
                     const InitialDistribution &initialdistribution_,
                     const ParametersMonteCarlo &parameters_montecarlo_,
                     TrajectoryLogger &trajectory_logger_,
                     const bool verbose_=false) :
    Base(distribution_,
         timestep_,
         qoi_,
         initialdistribution_,
         parameters_montecarlo_,
         verbose_), N(0), 
    trajectory_logger(trajectory_logger_),
    estimator(qoi_.N()) {
    measureCostPerTimestep();
    measureCostPerQoIEvaluation();
    distribution.reset();
  }

  // Run the integrator
  virtual void run() {
    t_run.reset();
    t_run.start();
    unsigned int M = parameters_montecarlo.M();
    double epsilon = parameters_montecarlo.epsilon();
    double h_uniform = parameters_montecarlo.h();
    double h_adaptive = h_uniform;
    bool adaptive = parameters_montecarlo.adaptive();
    double T = parameters_montecarlo.T();
    double time = T - 1.e-12; // makes sure that the while loop will not exceed T due to rounding errors
    double X;
    double U;
    estimator.reset();
    // Initial guess for number of samples
    N = 1000;
    bool sufficientStat = false;
    trajectory_logger.open();
    do {
      // Loop over particles
      unsigned int N_0 = estimator.samples();
      for (unsigned int i=N_0;i<N;++i) {
        // Loop over timesteps
        initialdistribution.draw(U,X);
        double S_standard = 1.0;
        trajectory_logger.start_trajectory();
        trajectory_logger.add(0,X,U);
        double t = 0.0;
        int j = 1;
        while (t < time) {
          double xi[Timestep::nxi];
          distribution.draw(Timestep::nxi,xi);
          if (adaptive) {
            h_adaptive = timestep.adapt(h_uniform,X);
            h_adaptive = std::min(h_adaptive,T-t);
          }
          timestep.step(xi,h_adaptive,U,X,S_standard);
          trajectory_logger.add(j,X,U);
          j++;
          t+=h_adaptive;
        }
        trajectory_logger.finish_trajectory();
        estimator.accumulate(qoi.evaluate(X,U));
      }
      N = (unsigned int) ceil(2./(epsilon*epsilon)*estimator.max_variance());
      if (verbose) {
        std::cout << "   ...adjusting N to " << N << std::endl;
      }
      sufficientStat = (estimator.samples() >= N);
    } while (!sufficientStat);
    cost = estimator.samples()*(M*costTimestep+costQoI);
    t_run.stop();
    trajectory_logger.close();
  }

  /* Return expectation value of quantity of interest */
  virtual std::vector<double> E_QoI() const { return estimator.mean(); }

  /* Return variance of quantity of interest */
  virtual std::vector<double> Var_QoI() const { return estimator.variance(); }

  /* Return error of QoI */
  virtual std::vector<double> Error_QoI() const {
    std::vector<double> error = Var_QoI();
    for (size_t i=0; i<error.size(); ++i) {
      error[i] = std::sqrt(error[i]/N);
    }
    return error;
  }

  /* Return number of particles */
  virtual unsigned int Npart() const { return N; }

  /* print out results */
  virtual void show_results() const {
    std::cout.precision(5);
    std::cout << " h = " << std::scientific << parameters_montecarlo.h() << std::endl;
    std::cout.precision(10);
    for (size_t i=0; i<E_QoI().size(); ++i) {
      std::cout << " Expectation value" << "[" << i+1 << "] = " << std::fixed << E_QoI()[i];
      std::cout << " +/- " << Error_QoI()[i];
      std::cout << std::endl;
    }
    std::cout << " Number of part.      = " << N << std::endl;
    for (size_t i=0; i<Var_QoI().size(); ++i) {
      std::cout << " Variance " << "[" << i+1 << "]         = " << Var_QoI()[i] << std::endl;
    }
    std::cout << std::endl;
    std::cout.precision(5);
    std::cout << " Cost:" << std::endl;
    std::cout << "   cost per timestep = ";
    std::cout.precision(5);
    std::cout << std::fixed << costTimestep*1.E9 << " ns ";
    std::cout << std::endl;
    std::cout << "   cost per quantity of interest evaluation = ";
    std::cout.precision(5);
    std::cout << std::fixed << costQoI*1.E9 << " ns ";
    std::cout << std::endl;
    std::cout << "   cost total = ";
    std::cout << std::scientific << Base::Cost() << std::endl;
    std::cout << std::endl;
    std::cout << " Elapsed time = " << std::scientific << t_run.elapsed() << " s" << std::endl;
    std::cout << std::endl;
  }

  /* ******************************************* *
   * Return cost of one time step
   * ******************************************* */
  double costPerTimestep() const {
    return costTimestep;
  }

  /* ******************************************* *
   * Return cost of evaluating and
   * accumulating the quantity of interest
   * ******************************************* */ 
  double costPerQoIEvaluation() const {
    return costQoI;
  }

private:
  /* ******************************************* *
   * Measure cost of time steps
   * ******************************************* */
  void measureCostPerTimestep() {
    unsigned int nSteps = (1 << 10); // Calculate 2^10 ~ 1000
    // Fine level
    double h_uniform = parameters_montecarlo.h();
    double U;
    double X;
    double t_total = 0.0;
    while (t_total < 1.0) {
      nSteps *= 2;
      initialdistribution.draw(U,X);
      double S_standard = 1.0;
      Timer t_step;
      t_step.start();
      for (unsigned int i=0;i<nSteps;++i) {
        double xi[Timestep::nxi];
        distribution.draw(Timestep::nxi,xi);
        timestep.step(xi,h_uniform,U,X,S_standard);
      }
      t_step.stop();
      t_total = t_step.elapsed();
    }
    costTimestep = t_total/double(nSteps);
    // DO NOT REMOVE THIS LINE: It ensure that the code is actually compiled
    // and executed to get the timings
    X_tmp = X;
  }

  /* ******************************************* *
   *  Measure cost of evaluating and 
   *  accumulating the quantity of interest
   *  ******************************************* */
  void measureCostPerQoIEvaluation() {
    unsigned int nSamples = (1 << 10); // Calculate 2^10 = 1024
    double U;
    double X;
    double t_total = 0.0;
    while (t_total < 1.0) {
      nSamples *= 2;
      initialdistribution.draw(U,X);
      Timer t_step;
      t_step.start();
      for (unsigned int i=0;i<nSamples;++i) {
        estimator.accumulate(qoi.evaluate(X,U));
      }
      t_step.stop();
      t_total = t_step.elapsed();
    }
    costQoI = t_total/double(nSamples);
  }


private:
  using Base::distribution;
  using Base::timestep;
  using Base::qoi;
  using Base::initialdistribution;
  using Base::parameters_montecarlo;
  using Base::verbose;
  using Base::cost;
  using Base::messagehandler;
  using Base::t_run;
  Estimator estimator;
  unsigned int N;
  double costTimestep;
  double costQoI;
  double X_tmp;
  TrajectoryLogger& trajectory_logger;
};
#endif // STANDARDMC_HH
