/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the MLMCLangevin code.
 *  
 *  (c) Copyright Eike Mueller, Grigoris Katsiolides and Tony Shardlow, University of Bath
 *  
 *  Main Developer: Eike Mueller
 *  
 *  Contributors:
 *    Grigoris Katsiolides
 *    Tony Shardlow
 *  
 *  MLMCLangevin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MLMCLangevin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with MLMCLangevin (see files COPYING and COPYING.LESSER). If not,
 *  see <http://www.gnu.org/licenses/>.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/

#ifndef TIMESTEP_EULER_HH
#define TIMESTEP_EULER_HH TIMESTEP_EULER_HH
#include <cmath>
#include <cassert>
#include "timestep_base.hh"
/* ************************************* *
 * Base class for one time step using
 * the splitting method combining the
 * Ornstein-Uhlenbeck process with an
 * integrator for the Hamiltonian
 * system.
 * ************************************* */
template <class L,bool Symplectic=true,int nRand=1>  // Number of random numbers per timestep
class TimestepEuler : public TimestepBase<L> {
public:
  typedef TimestepBase<L> Base;
  static const int nxi = nRand;
  /* Constructor */
  TimestepEuler(const L& langevin_) :
    Base(langevin_),
    h(1.0),
    sqrt2inv(1./sqrt(2)) {
  }

  // Combine increments
  inline void CombineIncrements(const double *I_fine,
                                double *I_coarse,
                                double &hi,
                                const double &X) const {
    I_coarse[0] = sqrt2inv*(I_fine[0]+I_fine[1]);
  }

  inline void CombineIncrementsAdaptive(double *DW,
                                        double *DW_coarse,
                                        double *DW_fine,
                                        const double &X_coarse,
                                        const double &X_fine,
                                        double time_diff) const {
    DW[0]*=std::sqrt(time_diff);
    DW_coarse[0]+=(DW[0]);
    DW_fine[0]+=DW[0];
  }

  inline void step(const double *xi,
                   double &hi,
                   double &U,
                   double &X,
                   double &S) const {
    double Uold = U;
    U -= (langevin.gradV(X,U) + langevin.lambda(X)*U)*hi;
    U += langevin.sigma(X)*S*xi[0]*sqrt(hi);
    if(Symplectic) {
      X += U*hi;
    } else {
      X += Uold*hi;
    }
    langevin.applyBoundaryConditions(X,U,S);
  }

  /* One time adaptive step: input random variables already have their 
     variance equal to the timestep size */
  inline void AdaptiveStep(const double *xi,
                           double &hi,
                           double &U,
                           double &X,
                           double &S) const {
    double Uold = U;
    U -= (langevin.gradV(X,U) + langevin.lambda(X)*U)*hi;
    U += langevin.sigma(X)*S*xi[0];
    if(Symplectic) {
      X += U*hi;
    } else {
      X += Uold*hi;
    }
    langevin.applyBoundaryConditions(X,U,S);
  }

protected:
  double h;                 // Time step size
  const double sqrt2inv;    // 1/sqrt(2)
  using Base::langevin;
};

#endif // TIMESTEP_EULER_HH
