/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the MLMCLangevin code.
 *  
 *  (c) Copyright Eike Mueller, Grigoris Katsiolides and Tony Shardlow, University of Bath
 *  
 *  Main Developer: Eike Mueller
 *  
 *  Contributors:
 *    Grigoris Katsiolides
 *    Tony Shardlow
 *  
 *  MLMCLangevin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MLMCLangevin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with MLMCLangevin (see files COPYING and COPYING.LESSER). If not,
 *  see <http://www.gnu.org/licenses/>.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/

#ifndef LANGEVIN_IDM_HH
#define LANGEVIN_IDM_HH LANGEVIN_IDM_HH

#include "langevin.hh"
#include "distribution.hh"
#include "parameters.hh"
#include <cmath>

/* ************************************************ *
 * Parameters for Inhomogeneous Dispersion Model
 * ************************************************ */
class ParametersIDM : public Parameters {
public:
  // Constructor
  ParametersIDM() :
    Parameters("inhomogeneousdispersionmodel"),
    regularise_(false),
    regbottom_(0.0),
    regtop_(0.0),
    ktau_(1.0),
    ksigma_(1.0),
    ustar_(1.0),
    adaptheight_(0.0),
    reflect_(false) {
    addKey("regularise",Bool);
    addKey("regbottom",Double);
    addKey("regtop",Double);
    addKey("ktau",Double);
    addKey("ksigma",Double);
    addKey("ustar",Double);
    addKey("adaptheight",Double);
    addKey("reflect",Bool);
  }

  // Read from file
  int readFile(const std::string filename) {
    int readSuccess = Parameters::readFile(filename);
    if (!readSuccess) {
      regularise_ = contents["regularise"]->getBool();
      regbottom_ = contents["regbottom"]->getDouble();
      regtop_ = contents["regtop"]->getDouble();
      ktau_ = contents["ktau"]->getDouble();
      ksigma_ = contents["ksigma"]->getDouble();
      ustar_ = contents["ustar"]->getDouble();
      adaptheight_ = contents["adaptheight"]->getDouble();
      reflect_ = contents["reflect"]->getBool();
    }
    return readSuccess;
  }

  // Return data
  bool regularise() const { return regularise_; }
  double regbottom() const { return regbottom_; }
  double regtop() const { return regtop_; }
  double ktau() const { return ktau_; }
  double ksigma() const { return ksigma_; }
  double ustar() const { return ustar_; }
  double adaptheight() const { return adaptheight_; }
  bool reflect() const { return reflect_; }


  // Class data
private:
  bool regularise_;
  double regbottom_;
  double regtop_;
  double ktau_;
  double ksigma_;
  double ustar_;
  double adaptheight_;
  bool reflect_;
};

/* **************************************** *
   Class representing a langevin process for
   the inhomogeneous dispersion model:

   V(X,U) = -1/2*[sigma_U^2(X) + U^2*log(sigma_U^2(X))]
   lambda(X) = 1/tau(X)
   sigma(X) = sqrt(2*sigma_U^2(X)/tau(X))
   sigma_U(X) = ksigma*ustar*(1 - X)^(3/4)
   tau(X) = ktau*X/sigma_U(X)

   * **************************************** */
class LangevinIDM : public LangevinBase {
public:
  typedef LangevinBase Base;
  // Constructor
  LangevinIDM(const ParametersIDM& parameters_idm_,
              const ParametersDistribution& parameters_dist_) :
    Base(parameters_dist_),
    parameters_idm(parameters_idm_),
    regularise(parameters_idm.regularise()),
    regbottom(parameters_idm.regbottom()),
    regtop(parameters_idm.regtop()),
    ktau(parameters_idm.ktau()),
    ksigma(parameters_idm.ksigma()),
    ustar(parameters_idm.ustar()),
    reflect(parameters_idm.reflect()) {
    regtopboundary = 1.0-regtop;
    twopi = 8.*atan(1.0);
    ksigmaUstar = ksigma*ustar;
    c1 = ksigma*ustar/ktau;
    c2 = 0.75*ksigmaUstar*ksigmaUstar;
    c3 = ksigmaUstar*sqrt(2.0*c1);
    r1 = ksigmaUstar*(pow(1.0 - regbottom,0.75))/(ktau*regbottom);
    r2 = ksigmaUstar*(pow(regtop,0.75))/(ktau*(1.0-regtop));
    r3 = ksigmaUstar*(pow(1.0 - regbottom,0.75))*sqrt(2.0*ksigmaUstar*(pow(1.0 - regbottom,0.75))/(ktau*regbottom));
    r4 = ksigmaUstar*(pow(regtop,0.75))*sqrt(2.0*ksigmaUstar*(pow(regtop,0.75))/(ktau*(1.0-regtop)));
    r5 = ksigmaUstar*(pow(1.0 - regbottom,0.75));
    r6 = ksigmaUstar*(pow(regtop,0.75));
    sigmaUXregbottom = ksigmaUstar*(pow(1.0 - regbottom,0.75));
    sigmaUXregtop = ksigmaUstar*(pow(regtop,0.75));
    logsigmaUXregbottom = log(sigmaUXregbottom);
    logsigmaUXregtop = log(sigmaUXregtop);
    l1 = - 0.5*sigmaUXregbottom*sigmaUXregbottom;
    l2 = - 0.5*sigmaUXregtop*sigmaUXregtop;
    C_adapt = lambda(parameters_idm.adaptheight());
  }

  // lambda
  inline double lambda(const double X) const {
    if(regularise) {
      if(X < regbottom) {
        return r1;
      } else if(X > regtopboundary) {
        return r2;
      }
    }
    return (c1*(pow(1.0 - X,0.75))/X);
  }
  // sigma
  inline double sigma(const double X) const {
    if(regularise) {
      if(X < regbottom) {
        return r3;
      } else if(X > regtopboundary) {
        return r4;
      }
    }
    return c3*(pow(1.0 - X,1.125))/sqrt(X);
  }

  // sigmaU
  inline double sigmaU(const double X) const {
    if(regularise) {
      if(X < regbottom) {
        return  r5;
      } else if(X > regtopboundary) {
        return r6;
      }
    }
    return ksigmaUstar*(pow(1.0 - X,0.75));
  }

  // Potential
  inline double V(const double X, const double U) const {
    if(regularise) {
      if(X < regbottom) {
        return ( l1 - U*U*logsigmaUXregbottom);
      } else if(X > regtopboundary) {
        return ( l2 - U*U*logsigmaUXregtop);
      }
    }
    double sigmaUX = ksigmaUstar*(pow(1.0 - X,0.75));
    return ( - 0.5*sigmaUX*sigmaUX - U*U*log(sigmaUX));
  }
  // Gradient of potential
  inline double gradV(const double X, const double U) const {
    if(regularise) {
      if(X < regbottom ) {
        return 0.0;
      } else if (X > regtopboundary) {
        return 0.0;
      }
    }
    return (c2*sqrt(1.0-X) + 0.75*U*U/(1.0-X));
  }

  inline double adaptTimestep(const double h_uniform,
                              const double &X) const {
    return std::min(h_uniform,h_uniform*C_adapt/lambda(X));
  }

  inline void applyBoundaryConditions(double& X,
                                      double& U,
                                      double& S) const {
    if (!reflect) return;
    if (X < 0) {
      X = -X;
      U = -U;
      S*=(-1.0);
    }
    if (X > 1.0) {
      X = 2.0-X;
      U = -U;
      S*=(-1.0);
    }
  }


protected:
  const ParametersIDM &parameters_idm;
  const bool regularise;
  const double regbottom;
  const double regtop;
  const double ktau;
  const double ksigma;
  const double ustar;
  const double reflect;
  double regtopboundary;
  double twopi;
  double ksigmaUstar;
  double r1;
  double r2;
  double r3;
  double r4;
  double r5;
  double r6;
  double c1;
  double c2;
  double c3;
  double sigmaUXregbottom;
  double sigmaUXregtop;
  double logsigmaUXregbottom;
  double logsigmaUXregtop;
  double l1;
  double l2;
  double C_adapt;
};

#endif // LANGEVIN_IDM_HH
