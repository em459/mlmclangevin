/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the MLMCLangevin code.
 *  
 *  (c) Copyright Eike Mueller, Grigoris Katsiolides and Tony Shardlow, University of Bath
 *  
 *  Main Developer: Eike Mueller
 *  
 *  Contributors:
 *    Grigoris Katsiolides
 *    Tony Shardlow
 *  
 *  MLMCLangevin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MLMCLangevin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with MLMCLangevin (see files COPYING and COPYING.LESSER). If not,
 *  see <http://www.gnu.org/licenses/>.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/

#ifndef LANGEVIN_HDM_HH
#define LANGEVIN_HDM_HH LANGEVIN_HDM_HH

#include "langevin.hh"
#include "distribution.hh"
#include "parameters.hh"

/* ************************************* *
 * Parameters for Homogeneous dispersion model
 * ************************************* */
class ParametersHDM : public Parameters {
public:
  // Constructor
  ParametersHDM() :
    Parameters("homogeneousdispersionmodel"),
    tau_(1.0),
    sigmaU_(1.0),
    reflect_(false) {
    addKey("tau",Double);
    addKey("sigmaU",Double);
    addKey("reflect",Bool);
  }

  // Read from file
  int readFile(const std::string filename) {
    int readSuccess = Parameters::readFile(filename);
    if (!readSuccess) {
      tau_ = contents["tau"]->getDouble();
      sigmaU_ = contents["sigmaU"]->getDouble();
      reflect_ = contents["reflect"]->getBool();
    }
    return readSuccess;
  }

  // Return data
  double tau() const { return tau_; }
  double sigmaU() const { return sigmaU_; }
  bool reflect() const { return reflect_; }

  // Class data
private:
  double tau_;
  double sigmaU_;
  bool reflect_;
};

/* **************************************** *
   Class representing a langevin process for
   the harmonic oscillator:

   lambda(X) = 1/tau = const.
   sigma(X) = \sqrt{2*sigma_U^2/tau} = const.

   * **************************************** */
class LangevinHDM : public LangevinBase {
public:
  typedef LangevinBase Base;
  // Constructor
  LangevinHDM(const ParametersHDM& parameters_hdm_,
              const ParametersDistribution& parameters_dist_) :
    Base(parameters_dist_),
    parameters_hdm(parameters_hdm_),
    lambda0(1.0/parameters_hdm.tau()),
    sigma0(sqrt(2./parameters_hdm.tau())*parameters_hdm.sigmaU()),
    reflect(parameters_hdm.reflect()) {}

  // lambda
  inline double lambda(const double X) const {
    return lambda0;
  }
  // sigma
  inline double sigma(const double X) const {
    return sigma0;
  }

  inline void applyBoundaryConditions(double& X,
                                      double& U,
                                      double& S) const {
    if (!reflect) return;
    if (X < 0) {
      X = -X;
      U = -U;
      S*=(-1.0);
    }
    if (X > 1.0) {
      X = 2.0-X;
      U = -U;
      S*=(-1.0);
    }
  }

protected:
  const ParametersHDM &parameters_hdm;
  const double lambda0;
  const double sigma0;
  const bool reflect;
};

#endif // LANGEVIN_HDM_HH
