### COPYRIGHT AND LICENSE STATEMENT ###
#
#  This file is part of the MLMCLangevin code.
#  
#  (c) Copyright Eike Mueller, Grigoris Katsiolides and Tony Shardlow,
#      University of Bath
#  
#  Main Developer: Eike Mueller
#  
#  Contributors:
#    Grigoris Katsiolides
#    Tony Shardlow
#  
#  MLMCLangevin is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#  
#  MLMCLangevin is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.
#  
#  You should have received a copy of the GNU Lesser General Public License
#  along with MLMCLangevin (see files COPYING and COPYING.LESSER). If not,
#  see <http://www.gnu.org/licenses/>.
#
### COPYRIGHT AND LICENSE STATEMENT ###

##################################################
# General parameters
##################################################
general:
  do_standardmc = false    # Run standard MC?
  do_distbiasmc = false   # Run distribution bias MC?
  do_multilevelmc = true  # Run multilevel MC?

##################################################
# Random number generator parameters
##################################################
distribution:
  seed = 3523525 # negative values -> use system time

##################################################
# Monte Carlo parameters
##################################################
montecarlo:
  T = 1.0                 # Final timestep
  epsilon = 1.e-4         # Tolerance epsilon
  Mcoarse = 10            # Timestep on coarsest multigrid level
  L = 4                   # Finest multigrid level (# timesteps = Mcoarse*2^L)
  adaptive = false        # Adaptive timesteps?

##################################################
# Multilevel parameters
##################################################
multilevel:
  coarseexact = false     # Exact solution on coarsest level?
  Noversample = 1         # Oversampling factor
  Nmin = 500              # minimal number of samples
  ell0 = 0                # Coarsest multigrid level

##################################################
# Parameters for boundary conditions
##################################################
boundarycondition:


##################################################
# Parameters for harmonic oscillator
##################################################
harmonicoscillator:
  omega = 1.0             # Angular frequency
  lambda = 1.0            # Damping
  sigma = 1.0             # Noise

##################################################
# Parameters for quartic oscillator
##################################################
quarticoscillator:
  x0 = 1.0                # Position of minimum
  omega = 1.0             # Angular frequency at minimum
  lambda = 1.0            # Damping
  sigma = 1.0             # Noise

##################################################
# Parameters for inhomogeneous dispersion model
##################################################
inhomogeneousdispersionmodel:
  regularise = true       # Regularise 
  regbottom = 0.01        # Bottom regularisation layer thickness
  regtop = 0.01           # Top regularisation layer thickness
  ktau = 0.5              # Parameter k_tau
  ksigma = 1.3            # Parameter k_sigma
  ustar = 0.2             # Parameter u*
  adaptheight = 0.05      # adaptive height
  reflect = true          # Reflective BCs?

##################################################
# Parameters for homogeneous dispersion model
##################################################
homogeneousdispersionmodel:
  tau = 1.0               # velocity autocorrelation time
  sigmaU = 1.0            # velocity variance sigma_U
  reflect = true          # Reflective BCs?

##################################################
# Parameters for initial distribution
##################################################
initialdistributionfixed:
  U0 = 0.1                # Initial velocity
  X0 = 0.05                # Initial position

##################################################
# Parameters for QoI Indicator function
##################################################
qoiindicator:
  a = 0.1055                 # } interval [a,b]
  b = 0.1555                 # }

##################################################
# Parameters for QoI Gaussian
##################################################
qoigaussian:
  a = 0.0                 # } interval [a,b]
  b = 1.0                 # }

##################################################
# Parameters for QoI Smoothed Density
##################################################
qoismootheddensity:
  a = 0.1055              # } interval [a,b]
  b = 0.1555              # }
  r = 4                   # } must be 4, 6 or 8. Polynomial degree at most r+1
  delta = 0.1             # } smoothing parameter
  n = 2                   # } size of the QoI vector
