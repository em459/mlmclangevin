/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the MLMCLangevin code.
 *  
 *  (c) Copyright Eike Mueller, Grigoris Katsiolides and Tony Shardlow, University of Bath
 *  
 *  Main Developer: Eike Mueller
 *  
 *  Contributors:
 *    Grigoris Katsiolides
 *    Tony Shardlow
 *  
 *  MLMCLangevin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MLMCLangevin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with MLMCLangevin (see files COPYING and COPYING.LESSER). If not,
 *  see <http://www.gnu.org/licenses/>.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/

#ifndef QUANTITYOFINTEREST_HH
#define QUANTITYOFINTEREST_HH QUANTITYOFINTEREST_HH
#include <cmath>
#include <vector>
#include "parameters.hh"

/* *************************************** *
 * Particle position
 * *************************************** */
class QoIPosition {
public:
  // Constructor
  QoIPosition() :
    n(1) {}
  // Evaluate
  const inline std::vector<double> evaluate(const double X, const double U) const {
    std::vector<double> pos(n,X);
    return pos;
  }
  size_t N() const { return n; }

private:
  const size_t n;
};

/* ************************************* *
 * Parameters for Indicator function
 * ************************************* */
class ParametersQoIIndicator : public Parameters {
public:
  // Constructor
  ParametersQoIIndicator() :
    Parameters("qoiindicator"),
    a_(0.0),
    b_(1.0) {
    addKey("a",Double);
    addKey("b",Double);
  }

  // Read from file
  int readFile(const std::string filename) {
    int readSuccess = Parameters::readFile(filename);
    if (!readSuccess) {
      a_ = contents["a"]->getDouble();
      b_ = contents["b"]->getDouble();
      if (a_ >= b_) {
        std::cerr << "ERROR: interval [a,b] is empty" << std::endl;
        exit(-1);
      }
    }
    return readSuccess;
  }

  // Return data
  double a() const { return a_; }
  double b() const { return b_; }

  // Class data
private:
  double a_;
  double b_;
};


/* *************************************** *
 * Indicator function
 * *************************************** */
class QoIIndicator {
public:
  // Constructor
  QoIIndicator(const ParametersQoIIndicator &parameters_qoiindicator_) :
    parameters_qoiindicator(parameters_qoiindicator_),
    a(parameters_qoiindicator.a()),
    b(parameters_qoiindicator.b()),
    n(1) {}
  // Evaluate
  const inline std::vector<double> evaluate(const double X, const double U) const {
    std::vector<double> r(n,((a<X) && (X<b)));
    return r;
  }

  size_t N() const { return n; }

private:
  const ParametersQoIIndicator &parameters_qoiindicator;
  double a;
  double b;
  const size_t n;
};

/* ************************************* *
 * Parameters for Gaussian function
 * ************************************* */
class ParametersQoIGaussian : public Parameters {
public:
  // Constructor
  ParametersQoIGaussian() :
    Parameters("qoigaussian"),
    a_(0.0),
    b_(1.0) {
    addKey("a",Double);
    addKey("b",Double);
  }

  // Read from file
  int readFile(const std::string filename) {
    int readSuccess = Parameters::readFile(filename);
    if (!readSuccess) {
      a_ = contents["a"]->getDouble();
      b_ = contents["b"]->getDouble();
      if (a_ >= b_) {
        std::cerr << "ERROR: interval [a,b] is empty" << std::endl;
        exit(-1);
      }
    }
    return readSuccess;
  }

  // Return data
  double a() const { return a_; }
  double b() const { return b_; }

  // Class data
private:
  double a_;
  double b_;
};

/* *************************************** *
 * Gaussian
 * *************************************** */
class QoIGaussian {
public:
  // Constructor
  QoIGaussian(const ParametersQoIGaussian &parameters_qoigaussian_) :
    parameters_qoigaussian(parameters_qoigaussian_),
    a(parameters_qoigaussian.a()),
    b(parameters_qoigaussian.b()),
    mu(0.5*(a+b)), sigma2(0.25*(b-a)*(b-a)),
    n(1) {
    C = 1./sqrt(8.*atan(1.)*sigma2)*(b-a);
    sigma2halfinv = 0.5/sigma2;
  }
  // Evaluate
  const inline std::vector<double> evaluate(const double X, const double U) const {
    double d=X-mu;
    std::vector<double> gaussian(n,C*exp(-d*d*sigma2halfinv));
    return gaussian;
  }

  size_t N() const { return n; }

private:
  const ParametersQoIGaussian &parameters_qoigaussian;
  const double a;
  const double b;
  const double mu;
  const double sigma2;
  double sigma2halfinv;
  double C;
  const size_t n;
};

/* *************************************** *
 * Gaussian in velocity space
 * *************************************** */
class QoIGaussianVelocity {
public:
  // Constructor
  QoIGaussianVelocity(const ParametersQoIGaussian &parameters_qoigaussian_) :
    parameters_qoigaussian(parameters_qoigaussian_),
    a(parameters_qoigaussian.a()),
    b(parameters_qoigaussian.b()),
    mu(0.5*(a+b)), sigma2(0.25*(b-a)*(b-a)),
    n(1) {
    C = 1./sqrt(8.*atan(1.)*sigma2)*(b-a);
    sigma2halfinv = 0.5/sigma2;
  }
  // Evaluate
  const inline std::vector<double> evaluate(const double X, const double U) const {
    double d=U-mu;
    std::vector<double> gau_vel(n,C*exp(-d*d*sigma2halfinv));
    return gau_vel;
  }

  size_t N() const { return n; }

private:
  const ParametersQoIGaussian &parameters_qoigaussian;
  const double a;
  const double b;
  const double mu;
  const double sigma2;
  double sigma2halfinv;
  double C;
  const size_t n;
};

/* *************************************** *
 * TwoNorm
 * *************************************** */
class QoITwoNorm {
public:
  // Constructor
  QoITwoNorm() :
    n(1)  {}
  // Evaluate
  const inline std::vector<double> evaluate(const double X, const double U) const {
    std::vector<double> two_norm(n,X*X+U*U);
    return two_norm;
  }

  size_t N() const { return n; }

private:
  const size_t n;
};

/* *************************************** *
 * Parameters for Smoothed Density function
 * *************************************** */
class ParametersQoISmoothedDensity : public Parameters {
public:
  // Constructor
  ParametersQoISmoothedDensity() :
    Parameters("qoismootheddensity"),
    a_(0.0),
    b_(1.0),
    r_(4),
    delta_(0.1),
    n_(2) {
    addKey("a",Double);
    addKey("b",Double);
    addKey("r",Integer);
    addKey("delta",Double);
    addKey("n",Integer);
  }

  // Read from file
  int readFile(const std::string filename) {
    int readSuccess = Parameters::readFile(filename);
    if (!readSuccess) {
      n_ = contents["n"]->getInt();
      if (n_ < 1) {
        std::cerr << "ERROR: number of intervals has to be > 0" << std::endl;
        exit(-1);
      }
      a_ = contents["a"]->getDouble();
      b_ = contents["b"]->getDouble();
      r_ = contents["r"]->getInt();
      if (r_!=4 && r_!=6 && r_!=8) {
        std::cerr << "ERROR: r must be equal to 4, 6 or 8" << std::endl;
        exit(-1);
      }
      delta_ = contents["delta"]->getDouble();
      if (a_ >= b_) {
        std::cerr << "ERROR: interval [a,b] is empty" << std::endl;
        exit(-1);
      }
    }
    return readSuccess;
  }

  // Return data
  double a() const { return a_; }
  double b() const { return b_; }
  size_t r() const { return r_; }
  double delta() const { return delta_; }
  size_t n() const { return n_; }

  // Class data
private:
  double a_;
  double b_;
  size_t r_;
  double delta_;
  size_t n_;
};

/* ************************************* *
 * Smoothed Density function
 * ************************************* */
class QoISmoothedDensity {
public:
  // Constructor
  QoISmoothedDensity(const ParametersQoISmoothedDensity &parameters_qoismootheddensity_) :
    parameters_qoismootheddensity(parameters_qoismootheddensity_),
    a(parameters_qoismootheddensity.a()),
    b(parameters_qoismootheddensity.b()),
    r(parameters_qoismootheddensity.r()),
    delta(parameters_qoismootheddensity.delta()),
    n(parameters_qoismootheddensity.n()) {}
  // Evaluate a one-dimensional smoothing polynomial
  const inline double eval(const double X, const double U,const double a_input, const double b_input) const {
    double g1 = 0.0;
    double g2 = 0.0;
    double bm = b_input - delta;
    double bp = b_input + delta;
    double am = a_input - delta;
    double ap = a_input + delta;
    double b1 = (X - b_input)/delta;
    double a1 = (X - a_input)/delta;
    double b1sq = b1*b1;
    double a1sq = a1*a1;
    if (r == 4) {
      double c_0 = 0.5;
      double c_1 = 1.7578125;
      double c_3 = 2.7343750;
      double c_5 = 1.4765625;
      if (X < bm){
        g1 = 1.0;
      }
      else if ((X >= bm) && (X <= bp)) {
        g1 = c_0 + b1*(b1sq*(c_3 - c_5*b1sq) - c_1);
      }
      if (X < am){
        g2 = 1.0;
      }
      else if ((X >= am) && (X <= ap)) {
        g2 = c_0 + a1*(a1sq*(c_3 - c_5*a1sq) - c_1);
      }
    }
    else if (r == 6) {
      double c_0 = 0.5;
      double c_1 = 2.3925781;
      double c_3 = 7.1777344;
      double c_5 = 9.4746094;
      double c_7 = 4.1894531;
      if (X < bm){
        g1 = 1.0;
      }
      else if ((X >= bm) && (X <= bp)) {
        g1 = c_0 + b1*(b1sq*(c_3 + b1sq*(c_7*b1sq - c_5)) - c_1);
      }
      if (X < am){
        g2 = 1.0;
      }
      else if ((X >= am) && (X <= ap)) 
        g2 =c_0 + a1*(a1sq*(c_3 + a1sq*(c_7*a1sq - c_5)) - c_1);
    }
    else if (r == 8) {
      double c_0 = 0.5;
      double c_1 = 3.0281067;
      double c_3 = 14.8040771;
      double c_5 = 34.6415405;
      double c_7 = 35.3485107;
      double c_9 = 12.9829407;
      if (X < bm){
        g1 = 1.0;
      }
      else if ((X >= bm) && (X <= bp)) {
        g1 = c_0 + b1*(b1sq*(c_3 + b1sq*(b1sq*(c_7 - c_9*b1sq) - c_5)) - c_1);
      }
      if (X < am){
        g2 = 1.0;
      }
      else if ((X >= am) && (X <= ap))
        g2 = 0.5 + a1*(a1sq*(c_3 + a1sq*(a1sq*(c_7 - c_9*a1sq) - c_5)) - c_1);
    }
    return g1 - g2;
  }
  
  // Evaluate n_-dimensional quantity of interest
  const inline std::vector<double> evaluate(const double X, const double U) const {
    std::vector<double> con(n,0.0);
    double len = b - a;
    for (size_t i=0; i<con.size(); ++i) {
      con[i] = eval(X,U,a+len*i,b+len*i);
    }
    return con;
  }

  size_t N() const { return n; }

private:
  const ParametersQoISmoothedDensity &parameters_qoismootheddensity;
  double a;
  double b;
  const size_t r;
  double delta;
  const size_t n;
};

#endif // QUANTITYOFINTEREST_HH
