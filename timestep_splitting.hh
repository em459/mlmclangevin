/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the MLMCLangevin code.
 *  
 *  (c) Copyright Eike Mueller, Grigoris Katsiolides and Tony Shardlow, University of Bath
 *  
 *  Main Developer: Eike Mueller
 *  
 *  Contributors:
 *    Grigoris Katsiolides
 *    Tony Shardlow
 *  
 *  MLMCLangevin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MLMCLangevin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with MLMCLangevin (see files COPYING and COPYING.LESSER). If not,
 *  see <http://www.gnu.org/licenses/>.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/

#ifndef TIMESTEP_SPLITTING_HH
#define TIMESTEP_SPLITTING_HH TIMESTEP_SPLITTING_HH
#include <cmath>
#include <cassert>
#include "timestep_base.hh"
/* ************************************* *
 * Base class for one time step using
 * the splitting method combining the
 * Ornstein-Uhlenbeck process with an
 * integrator for the Hamiltonian
 * system.
 * ************************************* */
template <class L,int nRand=1>  // Number of random numbers per timestep
class TimestepSplitting : public TimestepBase<L> {
public:
  typedef TimestepBase<L> Base;
  static const int nxi = nRand;
  /* Constructor */
  TimestepSplitting(const L& langevin_) :
    Base(langevin_),
    h(1.0) {}

  // Combine increments
  inline void CombineIncrements(const double *I_fine,
                                double *I_coarse,
                                double &hi,
                                const double X) const {
    double mu=exp(-hi/double(nxi)*langevin.lambda(X));
    for (int i=0;i<nxi;++i) {
      I_coarse[i] = (mu*I_fine[2*i]+I_fine[2*i+1]) / sqrt(mu*mu+1.0);
    }
  }

  inline void CombineIncrementsAdaptive(double *DW,
                                        double *DW_coarse,
                                        double *DW_fine,
                                        const double &X_coarse,
                                        const double &X_fine,
                                        double time_diff) const {
    double lambda_coarse = langevin.lambda(X_coarse);
    double lambda_fine = langevin.lambda(X_fine);
    double m1 = exp(- lambda_coarse*time_diff);
    double m2 = exp(- lambda_fine*time_diff);
    DW_coarse[0]*=m1;
    DW_coarse[0]+=sqrt((1.0 - m1*m1)/(2.0*lambda_coarse))*DW[0];
    DW_fine[0]*=m2;
    DW_fine[0]+=sqrt((1.0 - m2*m2)/(2.0*lambda_fine))*DW[0];
  }

  /* One time step: first-order splitting: exact OU/symplectic Euler */
  inline void step(const double *xi,
                   double &hi,
                   double &U,
                   double &X,
                   double *S) const {};

protected:
  // Orstein-Uhlenbeck step for velocity
  inline void OUstep(double &U,
                     const double X,
                     const double h,
                     const double xi) const {
    double lambda = langevin.lambda(X);
    double mu = exp(-h*lambda);
    double alpha = sqrt(0.5*(1.-mu*mu)/lambda);
    U = mu*U + alpha*langevin.sigma(X)*xi;
  }

  /* One time adaptive step: input random variables already have their 
     variance equal to the timestep size */
  inline void OUstepAdaptive(double &U,
                             const double X,
                             const double h,
                             const double xi) const {
    double lambda = langevin.lambda(X);
    double mu = exp(-h*lambda);
    U = mu*U + langevin.sigma(X)*xi;
  }

protected:
  double h;                 // Time step size
  using Base::langevin;
};

#endif // TIMESTEP_SPLITTING_HH
