/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the MLMCLangevin code.
 *  
 *  (c) Copyright Eike Mueller, Grigoris Katsiolides and Tony Shardlow, University of Bath
 *  
 *  Main Developer: Eike Mueller
 *  
 *  Contributors:
 *    Grigoris Katsiolides
 *    Tony Shardlow
 *  
 *  MLMCLangevin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MLMCLangevin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with MLMCLangevin (see files COPYING and COPYING.LESSER). If not,
 *  see <http://www.gnu.org/licenses/>.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/

#ifndef PARAMETERS_HH
#define PARAMETERS_HH

#include<string>


#include <regex.h>

#include<iostream>
#include<cstdlib>
#include<fstream>
#include<map>

#include "parameters.hh"
#include "messages.hh"

/* ************************************************************** *
 * Base class representing any parameter
 * ************************************************************** */
class Parameter {
public:
  // Constructor
  Parameter(const std::string valueString) :
    valueString_(valueString) {}

  // Return in different data types
  virtual const double getDouble() const {
    std::cerr << " Invalid data type (double). " << std::endl;
    return 0.0;
  };
  virtual const int getInt() const {
    std::cerr << " Invalid data type. " << std::endl;
    return 0;
  };
  virtual const std::string getString() const {
    std::cerr << " Invalid data type. " << std::endl;
    return std::string("");
  };
  virtual const bool getBool() const {
    std::cerr << " Invalid data type. " << std::endl;
    return false;
  };
  // set data
  virtual void setValue(const std::string valueString) {
  }
private:
  std::string valueString_;
};

/* ************************************************************** *
 * Class representing a double parameter
 * ************************************************************** */
class doubleParameter : public Parameter {
public:
  doubleParameter(const std::string valueString) :
    Parameter(valueString) {
    value=atof(valueString.c_str());
  }
  virtual const double getDouble() const { return value; }
  virtual void setValue(const std::string valueString) {
    value=atof(valueString.c_str());
  }
private:
  double value;
};

/* ************************************************************** *
 * Class representing an integer parameter
 * ************************************************************** */
class intParameter : public Parameter {
public:
  intParameter(const std::string valueString) :
    Parameter(valueString) {
    value=atoi(valueString.c_str());
  }
  virtual const int getInt() const { return value; }
  virtual void setValue(const std::string valueString) {
    value=atoi(valueString.c_str());
  }
private:
  int value;
};

/* ************************************************************** *
 * Class representing a string parameter
 * ************************************************************** */
class stringParameter : public Parameter {
public:
  stringParameter(const std::string valueString) :
    Parameter(valueString) {
    value = valueString.substr(1, valueString.size()-2);
  }
  virtual const std::string getString() const { return value; }
  virtual void setValue(const std::string valueString) {
    value = valueString.substr(1, valueString.size()-2);
  }
private:
  std::string value;
};

/* ************************************************************** *
 * Class representing a boolean parameter
 * ************************************************************** */
class boolParameter : public Parameter {
public:
  boolParameter(const std::string valueString) :
    Parameter(valueString) {
    value = ( ( valueString == "true" ) ||
              ( valueString == "True" ) ||
              ( valueString == "TRUE" ) ) ;

  }
  virtual const bool getBool() const { return value; }
  virtual void setValue(const std::string valueString) {
    value = ( ( valueString == "true" ) ||
              ( valueString == "True" ) ||
              ( valueString == "TRUE" ) ) ;
  }
private:
  bool value;
};

/* ************************************************************** *
 *
 * Base class for parameters which can be read from file.
 *
 * The file has to have the following structure:
 *
 * sectionname:
 *   key = value
 *   key = value
 *   ...
 * sectionname:
 *   key = value
 *   key = value
 *   ...
 *
 * Blank lines and lines starting with '#' are ignored.
 *
 * ************************************************************** */
class Parameters {
public:
  enum Datatype {Integer, Double, String, Bool};
  friend std::ostream& operator<<(std::ostream& output, const Parameters& p);

  /* ************************************************************** *
   * Constructor.
   * ************************************************************** */
  Parameters(const std::string section);

  /* ************************************************************** *
   * Read from stream (go back to beginning of stream) and return
   * 0 if everything is ok, 1 otherwise
   * ************************************************************** */
  int readFile(const std::string filename);

protected:
  void addKey(const std::string key, const Datatype datatype);
  std::map<std::string,Parameter*> contents;
private:
  std::map<std::string,Datatype> keywords;
  std::string section_;
  regex_t regexComment_;
  regex_t regexKeyword_;
  regex_t regexKeyValueAny_;
  regex_t regexKeyValueInt_;
  regex_t regexKeyValueDouble_;
  regex_t regexKeyValueBool_;
  regex_t regexKeyValueString_;
  static const bool verbose=0;
protected:
  const MessageHandler messagehandler;
};

/* ************************************************************** *
 * Write to stream
 * ************************************************************** */

std::ostream& operator<<(std::ostream& output, const Parameters& p);

#endif // PARAMETERS_HH
