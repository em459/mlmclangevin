#ifndef DIAGONAL_MATRIX_HH
#define DIAGONAL_MATRIX_HH DIAGONAL_MATRIX_HH

#include "vector.hh"

template <int dim>
class DiagonalMatrix {
public:

  DiagonalMatrix() {
    std::fill(matrix_data,matrix_data+dim,0.0);
  }

  DiagonalMatrix(std::vector<double> a) {
    std::copy(a.begin(),a.end(),matrix_data);
  }

  typedef DiagonalMatrix<dim> DiagonalMatrixType;
  typedef Vector<dim> VectorType;
 
  DiagonalMatrixType & operator+=(const DiagonalMatrixType &other) {
    for (int i=0;i<dim;++i) {
      matrix_data[i] += other.matrix_data[i];
    }
    return *this;
  }

  // Add two matrices
  friend DiagonalMatrixType operator+(const DiagonalMatrixType& m,
                                      const DiagonalMatrixType& n) {
    DiagonalMatrixType l;
    for (int i=0;i<dim;++i) {
      l.matrix_data[i]=m.matrix_data[i]+n.matrix_data[i];
    }
    return l;
  }

  // Add a double to a matrix
  friend DiagonalMatrixType operator+(const DiagonalMatrixType& m,
                                      const double v) {
    DiagonalMatrixType n;
    for (int i=0;i<dim;++i) {
      n.matrix_data[i]=m.matrix_data[i]+v;
    }
    return n;
  }

  // Add matrix to a double
  friend DiagonalMatrixType operator+(const double v,
                                      const DiagonalMatrixType& m) {
    DiagonalMatrixType n;
    for (int i=0;i<dim;++i) {
      n.matrix_data[i]=m.matrix_data[i]+v;
    }
    return n;
  }

  DiagonalMatrixType & operator-=(const DiagonalMatrixType &other) {
    for (int i=0;i<dim;++i) {
      matrix_data[i] -= other.matrix_data[i];
    }
    return *this;
  }

  // Subtract two matrices
  friend DiagonalMatrixType operator-(const DiagonalMatrixType& m,
                                      const DiagonalMatrixType& n) {
    DiagonalMatrixType l;
    for (int i=0;i<dim;++i) {
      l.matrix_data[i]=m.matrix_data[i]-n.matrix_data[i];
    }
    return l;
  }

  // Subtract a double from a matrix
  friend DiagonalMatrixType operator-(const DiagonalMatrixType& m,
                                      const double v) {
    DiagonalMatrixType n;
    for (int i=0;i<dim;++i) {
      n.matrix_data[i]=m.matrix_data[i]-v;
    }
    return n;
  }

  // Subtract a matrix from a double
  friend DiagonalMatrixType operator-(const double v,
                                      const DiagonalMatrixType& m) {
    DiagonalMatrixType n;
    for (int i=0;i<dim;++i) {
      n.matrix_data[i]=v-m.matrix_data[i];
    }
    return n;
  }

  friend VectorType operator*(const DiagonalMatrixType& m,
                                      const VectorType& v) {
    std::vector<double> w;
    std::vector<double> v_vec=v.as_vector();

    for (int i=0;i<dim;++i) {
      w.push_back(m.matrix_data[i]*v_vec[i]);
    }
  VectorType w_(w);
  return w_;
  }

  DiagonalMatrixType & operator*=(const double v) {
    for (int i=0;i<dim;++i) {
      matrix_data[i]*=v;
    }
    return *this;
  }

  // Multiply a matrix with a double
  friend DiagonalMatrixType operator*(const double v,
                                      const DiagonalMatrixType& m) {
    DiagonalMatrixType w;
    for (int i=0;i<dim;++i) {
      w.matrix_data[i]=m.matrix_data[i]*v;
    }
    return w;
  }

  // Multiply a double with a matrix
  friend DiagonalMatrixType operator*(const DiagonalMatrixType& m,
                                      const double v) {
    DiagonalMatrixType w;
    for (int i=0;i<dim;++i) {
      w.matrix_data[i]=m.matrix_data[i]*v;
    }
    return w;
  }

  DiagonalMatrixType & operator/=(const double v) {
    for (int i=0;i<dim;++i) {
      matrix_data[i]/=v;
    }
    return *this;
  }

  // Division by a double
  friend DiagonalMatrixType operator/(const DiagonalMatrixType& m,
                                      const double v) {
    DiagonalMatrixType w;
    for (int i=0;i<dim;++i) {
      w.matrix_data[i]=m.matrix_data[i]/v;
    }
    return w;
  }

  DiagonalMatrixType & operator=(const DiagonalMatrixType &m) {
    for (int i=0;i<dim;++i) {
      matrix_data[i] = m.matrix_data[i];
    }
    return *this;
  }

  // Return as std::vector
  std::vector<double> as_vector() const {
    std::vector<double> v(dim);
    std::copy(matrix_data,matrix_data+dim,v.begin());
    return v;
  }

private:
  double matrix_data[dim];
};

#endif // DIAGONAL_MATRIX_HH
