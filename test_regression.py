import sys
import shutil
import tempfile
import subprocess
from contextlib import contextmanager
import glob
import os
from collections import OrderedDict
import datetime
import argparse
import re
import signal

# Codes for colouring output
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

class bcolors_none:
    HEADER = ''
    OKBLUE = ''
    OKGREEN = ''
    WARNING = ''
    FAIL = ''
    ENDC = ''
    BOLD = ''
    UNDERLINE = ''

# Auxilliary methods
def error_message(msg):
    '''Print a test-specific error message
        
    :arg msg: Message to format
    '''
    print '  '+bcolors.FAIL+'ERROR'+bcolors.ENDC+' : '+msg

def fatal_error_message(msg):
    '''Print test specific error message and abort
    
    :arg msg:
    '''
    error_message(msg)
    sys.exit(-1)

def timestamp(t):
    '''Convert time to time stamp string
    
    :arg t: time
    '''
    return '{:%d %b %Y %H:%M:%S}'.format(t)

def timediff(dt):
    '''Format as a time difference in minutes and seconds
    
    :arg dt: Time difference
    '''
    min, sec = divmod(dt.total_seconds(), 60)
    s = ''
    s += ('%4d' % int(min))+'m '
    s += ('%5.2f' % sec)+'s '
    return s
    

# Contextmanager for creating temporary directories which are automatically
# removed on exit
@contextmanager
def TmpDir():
    name = tempfile.mkdtemp()
    try:
        yield name
    finally:
        shutil.rmtree(name)

class ModelRun(object):
    '''Model run object. Provides functionality for running the model
    based on a specific instance of the source code and a particular test
    defined in the test directory.

    :arg tmp_dir: Temporary directory for compiling, running etc.
    :arg source_dir: Directory with main source code
    :arg test_dir: Directory with test specific data
    :arg log_dir: Any output (such as error files) is written to this directory
    :arg reference: Is this a reference run (used to add appropriate output)?
    '''
    def __init__(self,tmp_dir,source_dir,test_dir,log_dir,reference=False):
        '''Create new instance'''
        self._tmp_dir = tmp_dir
        if not os.path.exists(self._tmp_dir):
            os.makedirs(self._tmp_dir)
        self._source_dir = source_dir
        self._test_dir = test_dir
        self._log_dir = log_dir
        self._reference = reference
        if (self._reference):
            self._reference_label='reference '
        else:
            self._reference_label=''

    def compile(self):
        '''Compile the code'''
        t_start = datetime.datetime.now()
        print ' '+timestamp(t_start)+' : Starting '+self._reference_label+'compilation'
        stdout_file=os.path.join(self._log_dir,'stdout.txt')
        stderr_file=os.path.join(self._log_dir,'stderr.txt')
        sourcefiles = []
        for extension in ('*.hh','*.cc','Makefile'):
            sourcefiles += glob.glob(os.path.join(self._source_dir,
                                                  extension))
        for filename in sourcefiles:
            if os.path.isfile(filename):
                shutil.copy(filename, self._tmp_dir)
        try:
            shutil.copy(os.path.join(self._test_dir,'config.h'), self._tmp_dir)
        except:
            fatal_error_message('config.h not found.')
        with open(stdout_file,'w') as stdout:
            with open(stderr_file,'w') as stderr:
                cmd = ['make','all']
                success = False
                try:
                    p = subprocess.check_call(cmd,
                                              cwd=self._tmp_dir,
                                              stdout=stdout,
                                              stderr=stderr)
                    success = True
                except subprocess.CalledProcessError:
                    error_message('Compilation failed')
                    print '  See '+stdout_file+' and '+stderr_file+' for details.'
                    sys.exit(-1)
                except KeyboardInterrupt as e:
                    print 'Keyboard interrupt'
                    sys.exit(-1)
                except:
                    fatal_error_message('Compilation failed with unknown exception')
        t_finish = datetime.datetime.now()
        print ' '+timestamp(t_finish)+' : Finished '+self._reference_label+'compilation '+timediff(t_finish-t_start)
        print

    def run(self,algorithm,timeout_sec):
        '''Run the code for a particular algorithm

        This uses the parameter file parameter_algorithm.in and 
        produces the output files stdout_algorithm.txt and
        stderr_algorithm.txt in the test directory.
        The process will abort with a fatal error message it it runs for longer
        than the specified timeout
        
        :arg algorithm: Name of algorithm ('standard' or 'multilevel')
        :arg timeout: Timeout in seconds
        '''

        class TimeoutError(Exception):
            def __init__(self,value):
                self.value = value
            def __str__(self):
                return str(self.value)+' s'

        def timeout_handler(signum, frame):
            raise TimeoutError(timeout_sec)

        parameter_filename = 'parameters_'+algorithm+'.in'
        stdout_filename = 'stdout_'+algorithm+'.txt'
        stderr_filename = 'stderr_'+algorithm+'.txt'
        t_start = datetime.datetime.now()
        print ' '+timestamp(t_start)+' : Starting running '+self._reference_label+'code ('+algorithm+' MC)'
        try:
            shutil.copy(os.path.join(self._test_dir,parameter_filename), self._tmp_dir)
        except:
            fatal_error_message(parameter_filename+' not found.')
        stdout_file=os.path.join(self._log_dir,stdout_filename)
        stderr_file=os.path.join(self._log_dir,stderr_filename)
        with open(stdout_file,'w') as stdout:
            with open(stderr_file,'w') as stderr:
                cmd = ['./driver.x',parameter_filename]
                try:
                    if (timeout_sec > 0):
                        signal.signal(signal.SIGALRM, timeout_handler)
                        signal.alarm(timeout_sec)
                    p = subprocess.check_call(cmd,
                                              cwd=self._tmp_dir,
                                              stdout=stdout,
                                              stderr=stderr)
                except TimeoutError as e:
                    fatal_error_message('Exceeded time limit of '+str(e)+'  while running ./driver.x')
                except subprocess.CalledProcessError as e:
                    error_message('Failed to run ./driver.x')
                    print '  Error message: '+str(e)
                    print '  See '+stdout_file+' and '+stderr_file+' for details.'
                    sys.exit(-1)
                except KeyboardInterrupt as e:
                    print 'Keyboard interrupt'
                    sys.exit(-1)
                except:
                    fatal_error_message('Failed to run ./driver.x with unknown exception')
                finally:
                    signal.alarm(0)
                shutil.copy(stdout_file,self._tmp_dir)
        t_finish = datetime.datetime.now()
        print ' '+timestamp(t_finish)+' : Finished running '+self._reference_label+'code '+timediff(t_finish-t_start)
        print


class RegressionTest(object):
    '''Regression test object. Provides functionality for carrying out an
    regression test for a specific source directory and test directory.
    The test directory contains a specific config.h file, input files 
    (named parameters_standard.in and parameters_multilevel.in) and the
    reference output in reference_standard.txt and reference_multilevel.txt
    to compare to. If the reference output files don't exist, the source
    code in the reference source dir will be compiled and run to generate them.
    All output is logged in a specific log directory.
    A temporary directory has to be created for executing the tests.

    :arg tmp_dir: Temporary directory for compiling, running etc.
    :arg source_dir: Directory with main source code
    :arg reference_source_dir: Directory with reference source code
    :arg test_dir: Directory with test specific data
    :arg log_dir: Any output (such as error files) is written to this directory
    '''
    
    def __init__(self,
                 tmp_dir,
                 source_dir,
                 reference_source_dir,
                 test_dir,
                 log_dir):
        '''Create new instance'''
        self._tmp_dir = tmp_dir
        self._source_dir = source_dir
        self._reference_source_dir = reference_source_dir
        self._test_dir = test_dir
        self._log_dir = log_dir
        self._tolerance = 1.E-12
        self._modelrun = ModelRun(os.path.join(self._tmp_dir,'current'),
                                  self._source_dir,
                                  self._test_dir,
                                  self._log_dir)
        # Check if reference output files exist
        self._reference_output_exists = {}
        for algorithm in ('standard','multilevel'):
            if os.path.isfile(os.path.join(self._test_dir,
                                            'stdout_reference_'+algorithm+'.txt')):
                self._reference_output_exists[algorithm] = True
            else:
                self._reference_output_exists[algorithm] = False
                print '  Reference output does not exit for '+algorithm+' MC and will be recreated'
        if not all(self._reference_output_exists.values()):
            self._reference_modelrun = ModelRun(os.path.join(self._tmp_dir,'reference'),
                                                self._reference_source_dir,
                                                self._test_dir,
                                                self._log_dir,
                                                reference=True)
                               

    def compile(self):
        '''Compile code.
        
        If none of the reference output files exists, also compile
        the reference code.'''
        if not all(self._reference_output_exists.values()):
            self._reference_modelrun.compile()
        self._modelrun.compile()

    def run(self,algorithm,timeout_sec):
        '''Run code for a specific algorithm ('standard' or 'multilevel').

        If the corresponding reference output file doesn't exist, also run
        the reference code.

        :arg algorithm: Algorithm to run
        :arg timeout_sec: Timeout in seconds
        '''
        if (not self._reference_output_exists[algorithm]):
            self._reference_modelrun.run(algorithm,timeout_sec)
        self._modelrun.run(algorithm,timeout_sec)

    def _extract_results(self,filename,algorithm):
        '''Extract the results from a given file and for a particular
        algorithm (multilevel or single level) and return the
        expectation value and variance

        :arg filename: Name of file to parse
        :arg algorithm: Name of algorithm ('standard' or 'multilevel')
        '''
        alg_label = {'standard':'STANDARD MC',
                     'multilevel':'MULTILEVEL MC'}
        regex_float = '[\+\-]?[0-9]+\.[0-9]+(?:[eE][\+\-][0-9]+){0,1}'
        parse = False
        value = None
        error = None
        with open(filename) as f:
            for line in f.readlines():
                m = re.match('^ *\*\*\* *([a-zA-Z_ ]+) *\*\*\* *$',line)
                if m:
                    parse = (m.group(1).strip()==alg_label[algorithm].strip())
                m = re.match('^ * Expectation value\[1\] *= *('+regex_float+') * \+\/\- *('+regex_float+') *$',line)
                if (m and parse):
                    value = float(m.group(1))
                    error = float(m.group(2))
        if (value == None):
            fatal_error_message('Can not find result for '+algorithm+' MC in file '+filename)
        return value, error

    def _compare_results(self,value,error,value_ref,error_ref):
        '''Compare results given as value +/- error
        Print out the results and differences. Returns two boolean numbers
        which indicate whether the numbers match exactly and statistically
        (within 2 standard deviations).


        :arg value: Value 
        :arg error: Statistical error (standard deviation)
        :arg value_ref: Reference value
        :arg error_ref: Statistical error (standard deviation) of reference
                        result.
        '''
        print '    output    = '+('%12.6e' % value)+' +/- '+('%12.6e' % error)
        print '    reference = '+('%12.6e' % value_ref)+' +/- '+('%12.6e' % error_ref)
        delta = abs(value - value_ref)
        delta_sigma = delta/(error_ref)
        s = '    '
        s += 'delta       = '+('%12.6e' % delta)
        if ((delta < self._tolerance) and (abs(error-error_ref)<self._tolerance)):
            print s+bcolors.OKGREEN+' PASS'+bcolors.ENDC
            exact_match = True
        else:
            print s+bcolors.FAIL+' FAIL'+bcolors.ENDC
            exact_match = False
        s = '    '
        s += 'delta/sigma = '+('%12.6e' % delta_sigma)
        if (delta_sigma < 2.0):
            print s+bcolors.OKGREEN+' PASS'+bcolors.ENDC
            statistical_match = True
        else:
            print s+bcolors.FAIL+' FAIL'+bcolors.ENDC
            statistical_match = False
        return exact_match, statistical_match
            
    def check_results(self,algorithm):
        '''Compare the results in the output file to the results in the
        corresponding reference file. Returns two boolean numbers
        which indicate whether the numbers match exactly and statistically
        (within 2 standard deviations).

        :arg algorithm: algorithm to compare ('standard' or 'multilevel')
        '''
        t_start = datetime.datetime.now()
        print ' '+timestamp(t_start)+' : Comparing results'

        stdout_filename = 'stdout_'+algorithm+'.txt'
        output_file = os.path.join(self._tmp_dir,'current',stdout_filename)
        # Copy reference file if it doesn't exit
        stdout_reference_filename = 'stdout_reference_'+algorithm+'.txt'
        reference_file = os.path.join(self._test_dir,stdout_reference_filename)
        if (not self._reference_output_exists[algorithm]):
            shutil.copy(os.path.join(self._tmp_dir,
                                     'reference',
                                     stdout_filename),
                        reference_file)
        print '  algorithm : '+algorithm
        value, error = self._extract_results(output_file,algorithm)
        value_ref, error_ref = self._extract_results(reference_file,algorithm)
        passed_exact, passed_statistical = self._compare_results(value,
                                                                 error,
                                                                 value_ref,
                                                                 error_ref)
        if (not passed_exact) or (not passed_statistical):
            print '    Copied output file to '+os.path.join(self._test_dir,stdout_filename)+' for further inspection'
            shutil.copy(output_file, self._test_dir)
        print
        return passed_exact, passed_statistical

class TestHarness(object):
    '''Test harness for running a collection of tests
    
    :arg source_dir: Directory with source code
    :arg reference_source_dir: Directory with reference source code
    :arg test_root_dir: Root directory for tests
    :arg log_dir: Output (log) directory
    :arg timeout_sec: Timeout for running code (in seconds)
    :arg run_standard_mc: Run standard Monte Carlo algorithm?
    :arg run_multigrid_mc: Run multilevel Monte Carlo algorithm?
    '''
    def __init__(self,
                 source_dir,
                 reference_source_dir,
                 test_root_dir,
                 log_dir,
                 timeout_sec,
                 run_standard_mc,
                 run_multilevel_mc):
        self._source_dir = source_dir
        self._reference_source_dir = reference_source_dir
        self._test_root_dir = test_root_dir
        self._log_dir = log_dir
        self._timeout_sec = timeout_sec
        self._tests = OrderedDict()
        self._test_dirs = []
        self._run_standard_mc = run_standard_mc
        self._run_multilevel_mc = run_multilevel_mc
        self._algorithms = []
        if (self._run_standard_mc):
            self._algorithms.append('standard')
        if (self._run_multilevel_mc):
            self._algorithms.append('multilevel')

    def add_test_dir(self,test_dir):
        '''Add a test to collection.

        :arg test_dir: Directory containing test
        '''
        self._test_dirs.append(test_dir)

    def run_all(self):
        '''Run all tests'''
        for i,dir in enumerate(self._test_dirs):
            s = bcolors.OKBLUE
            s += 'Test '+('%3d' % (i+1))+' of '+('%3d' % len(self._test_dirs))+' : '
            s += dir
            s += bcolors.ENDC
            print s
            with TmpDir() as tmp_dir:
                test = RegressionTest(tmp_dir,
                                      self._source_dir,
                                      self._reference_source_dir,
                                      os.path.join(self._test_root_dir,dir),
                                      self._log_dir)
                test.compile()
                for algorithm in self._algorithms:
                    test.run(algorithm,self._timeout_sec)
                    exact_label = {True:'exact',False:'statistical'}
                    matches = {}
                    matches['exact'], matches['statistical'] = test.check_results(algorithm)
                    for label, match in matches.iteritems():
                        s = dir+' '+('%-12s' % algorithm)+' ['+('%11s' % label)+']'
                        self._tests[s] = match
                
    def show_summary(self):
        '''Print out summary of results'''
        print bcolors.BOLD+'=== Summary ==='+bcolors.ENDC
        n_pass = 0
        n_fail = 0
        n_total = 0
        for i,test in enumerate(self._tests.keys()):
            mod = 2*len(self._algorithms)
            i_major = i/mod
            i_minor = i % mod
            s = '  '+('%-3d' % (i_major+1))+'. '+('%-3d' % (i_minor+1))
            s += ' '+('%60s' % test)
            if (self._tests[test]):
                s += bcolors.OKGREEN+' PASS'+bcolors.ENDC
                n_pass += 1
            else:
                s += bcolors.FAIL+' FAIL'+bcolors.ENDC
                n_fail += 1
            print s
        n_total = n_pass+n_fail
        print
        # Print out summary
        if (n_fail == 0):
            print bcolors.OKGREEN+' all '+str(n_total)+' tests PASSED'+bcolors.ENDC
        else:
            if (n_fail > 0):
                print bcolors.FAIL+' '+str(n_fail)+' of '+str(n_total)+' tests FAILED'+bcolors.ENDC
        print

#########################################################################
# M A I N
#########################################################################
if (__name__ == '__main__'):
    # Parse command line arguments
    parser = argparse.ArgumentParser(description='Automated regression testing')
    parser.add_argument('--source_dir',
                        required=False,
                        default='.',
                        dest='source_dir',
                        help='Directory with source code')
    parser.add_argument('--reference_source_dir',
                        required=True,
                        default='.',
                        dest='reference_source_dir',
                        help='Directory with reference source code')
    parser.add_argument('--test_dir',
                        required=False,
                        default='regression_tests',
                        dest='test_dir',
                        help='Directory with regression tests')
    parser.add_argument('--log_dir',
                        required=False,
                        default='.',
                        dest='log_dir',
                        help='Directory for logging output')
    parser.add_argument('--timeout',
                        required=False,
                        type=int,
                        default=600,
                        dest='timeout_sec',
                        help='Timeout in seconds (0 = no timeout)')
    parser.add_argument('--only',
                        required=False,
                        default=None,
                        dest='only',
                        help='Only run selected tests. Can be a single test (--only=test_A) or a list of tests (--only=test_A,test_B,test_C)')
    parser.add_argument('--no-color',
                        action='store_true',
                        dest='nocolor',
                        default=False,
                        help='Do not use color highlighting')
    parser.add_argument('--standard-only',
                        action='store_false',
                        dest='run_multilevel',
                        default=True,
                        help='Only run standard MC algorithm')
    parser.add_argument('--multilevel-only',
                        action='store_false',
                        dest='run_standard',
                        default=True,
                        help='Only run multilevel MC algorithm')
    parser.add_argument('--list-only',
                        action='store_true',
                        dest='list_only',
                        default=False,
                        help='Only list tests, do not run them')
    args = parser.parse_args()
    # Disable colors if requested
    if (args.nocolor):
        bcolors = bcolors_none
    # Work out which MC algorithms to run
    print bcolors.BOLD+'=== Regression testing ==='+bcolors.ENDC
    print ' source directory           = '+args.source_dir
    print ' reference source directory = '+args.reference_source_dir
    print ' test root directory        = '+args.test_dir
    print ' log directory              = '+args.log_dir
    print ' timeout                    = '+str(args.timeout_sec)+' s'
    print ' run standard MC?           = '+str(args.run_standard)
    print ' run multilevel MC?         = '+str(args.run_multilevel)
    print
    print ' run \"python '+sys.argv[0]+' --help\" to list all options'
    print
    if (not (args.run_standard or args.run_multilevel)):
        print bcolors.FAIL+'ERROR: need to run at least one MC algorithm, do not use --standard-only and --multilevel-only together.'+bcolors.ENDC
        sys.exit(-1)

    # Find all test directories
    potential_test_dirs = glob.glob(os.path.join(args.test_dir,'*'))
    test_dirs = []
    for dir in potential_test_dirs:
        if os.path.isdir(dir):
            test_dirs.append(os.path.basename(dir))
    test_harness = TestHarness(args.source_dir,
                               args.reference_source_dir,
                               args.test_dir,
                               args.log_dir,
                               args.timeout_sec,
                               args.run_standard,
                               args.run_multilevel)
    print 'Tests found in '+args.test_dir+' :'
    for i,dir in enumerate(test_dirs):
        print '  '+('%3d' % (i+1))+' '+dir
    print
    if (args.list_only):
        sys.exit(0)
    # Add all tests
    if (args.only == None):
        for i,dir in enumerate(test_dirs):
            test_harness.add_test_dir(dir)
        print ' running all tests'
    else:
        # Extract all tests from list
        candidate_tests = args.only.split(',')
        for test in candidate_tests:
            if (not test in test_dirs):
                print bcolors.FAIL+'ERROR: \"'+test+'\" not found'+bcolors.ENDC
                sys.exit(-1)
            else:
                test_harness.add_test_dir(test)
        print ' Only running the following tests:'
        for i,dir in enumerate(candidate_tests):
            print '  '+('%3d' % (i+1))+' '+dir
    print
        
    test_harness.run_all()
    test_harness.show_summary()
