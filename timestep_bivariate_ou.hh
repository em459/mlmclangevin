/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the MLMCLangevin code.
 *  
 *  (c) Copyright Eike Mueller, Grigoris Katsiolides and Tony Shardlow, University of Bath
 *  
 *  Main Developer: Eike Mueller
 *  
 *  Contributors:
 *    Grigoris Katsiolides
 *    Tony Shardlow
 *  
 *  MLMCLangevin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MLMCLangevin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with MLMCLangevin (see files COPYING and COPYING.LESSER). If not,
 *  see <http://www.gnu.org/licenses/>.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/

#ifndef TIMESTEP_BIVARIATE_OU_HH
#define TIMESTEP_BIVARIATE_OU_HH TIMESTEP_BIVARIATE_OU_HH
#include <cmath>
#include <cassert>
#include "timestep_base.hh"
/* ************************************* *
 * Class for one time step using a
 * bivariate Ornstein-Uhlenbeck process.
 * ************************************* */
template <class L,int nRand=2>  // Number of random numbers per timestep
class TimestepBivariateOU : public TimestepBase<L> {
public:
  static const int nxi = nRand;
  typedef TimestepBase<L> Base;
  /* Constructor */
  TimestepBivariateOU(const L& langevin_) :
    Base(langevin_), h(1.0) {
  }

  // Combine increments
  // I_fine = [r_U1,r_X1,r_U2,r_X2]
  //I_coarse = [r_U,r_X]
  inline void CombineIncrements(const double *I_fine,
                                double *I_coarse,
                                double &hi,
                                const double X) const {
    // Generate correlated random variables from I_fine
    double lambda=langevin.lambda(X);
    double sigma = langevin.sigma(X);
    double mu1 = exp(-hi*langevin.lambda(X));
    double mu2 = (1.0-mu1)/(langevin.lambda(X));
    double mu3 = 2.0*hi*lambda;
    double mu4 = exp(-mu3);
    double mu5 = sigma*sigma*0.5;
    double mu6 = 1.0/lambda;
    double varX = mu5*(mu6/(lambda*lambda))*(mu3 - 3.0 + 4.0*mu1 - mu4); 
    double varU = mu5*mu6*(1.0 - mu4);
    double covar = mu5*(mu6/lambda)*(1.0 - 2.0*mu1 + mu4);
    double rx1,ru1,rx2,ru2;
    ru1 = (covar/sqrt(varX))*I_fine[1] + sqrt(varU - covar*covar/varX)*I_fine[0];
    rx1 = sqrt(varX)*I_fine[1];
    ru2 = (covar/sqrt(varX))*I_fine[3] + sqrt(varU - covar*covar/varX)*I_fine[2];
    rx2 = sqrt(varX)*I_fine[3];
    //Combine the increments
    I_coarse[0] = (ru1*mu1 + ru2);
    I_coarse[1] = (rx1 + ru1*mu2 + rx2);
    correlate_xi = false;
  }

  inline void CombineIncrementsAdaptive(double *DW,
                                        double *DW_coarse,
                                        double *DW_fine,
                                        const double &X_coarse,
                                        const double &X_fine,
                                        double time_diff) const {}

  // xi = [r_U,r_X]
  inline void step(double *xi,
                   double &hi,
                   double &U,
                   double &X,
                   double &S) const {
    double rU;
    double rX;
    double sigma = langevin.sigma(X);
    double lambda = langevin.lambda(X);
    double grad = langevin.gradV(X,U);
    double mu1 = exp(-hi*lambda);
    double mu2 = (1.0-mu1)/lambda;
    if(correlate_xi) {
      double mu3 = 2.0*hi*lambda;
      double mu4 = exp(-mu3);
      double mu5 = sigma*sigma*0.5;
      double mu6 = 1.0/lambda;
      // Generate correlated random variables from xi
      double varX = mu5*(mu6/(lambda*lambda))*(mu3 - 3.0 + 4.0*mu1 - mu4);
      double varU = mu5*mu6*(1.0 - mu4);
      double covar = mu5*(mu6/lambda)*(1.0 - 2.0*mu1 + mu4);
      rU = (covar/sqrt(varX))*xi[1] + sqrt(varU - covar*covar/varX)*xi[0];
      rX = sqrt(varX)*xi[1];
    }
    else {
      rU = xi[0];
      rX = xi[1];
    }

    // timestep increment
    X += U*mu2 - grad*(hi - mu2)/lambda + S*rX;
    U = U*mu1 - mu2*grad + S*rU;
    langevin.applyBoundaryConditions(X,U,S);
    correlate_xi = true;
  }

protected:
  double h;                 // Time step size
  static bool correlate_xi;
  using Base::langevin;
};

template <typename L,int nRand>
bool TimestepBivariateOU<L,nRand>::correlate_xi = true;

#endif //TIMESTEP_BIVARIATE_OU_HH
