/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the MLMCLangevin code.
 *  
 *  (c) Copyright Eike Mueller, Grigoris Katsiolides and Tony Shardlow, University of Bath
 *  
 *  Main Developer: Eike Mueller
 *  
 *  Contributors:
 *    Grigoris Katsiolides
 *    Tony Shardlow
 *  
 *  MLMCLangevin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MLMCLangevin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with MLMCLangevin (see files COPYING and COPYING.LESSER). If not,
 *  see <http://www.gnu.org/licenses/>.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/

#ifndef DISTRIBUTION_DISCRETE_HH
#define DISTRIBUTION_DISCRETE_HH DISTRIBUTION_DISCRETE_HH

#include <cstdlib>
#include <vector>
#include <utility>
#include <set>
#include "distribution.hh"

/* ************************************* *
 * Discrete probability distribution
 * ************************************* */
class DistributionDiscrete : public Distribution {
public:
  typedef Distribution Base;
  DistributionDiscrete(const ParametersDistribution& parameters_dist_) :
    Base(parameters_dist_) {}
  // Number of points
  int nPoints() const { return dist.size(); }
  // Return probabilities as pairs of (value,probability)
  std::set<Configuration> getDistribution() const {
    return dist;
  }

  // Calculate the product distribution of n
  std::set<ProductConfiguration> productDistribution(const int n) const {
    std::set<ProductConfiguration> v;
    std::vector<double> w;
    double wgt=1.0;
    buildProductDistribution(n,0,&v,w,wgt);
    return v;
  }

protected:
  // Recursively build the product distribution
  void buildProductDistribution(int n, int j,
                                std::set<ProductConfiguration>* v,
                                std::vector<double> w,
                                double wgt) const {
    if (j == n) {
      v->insert(std::make_pair(w,wgt));
    } else {
      for (std::set<Configuration>::const_iterator it = dist.begin();
           it != dist.end(); ++it) {
        w.push_back(it->first);
        buildProductDistribution(n,j+1,v,w,wgt*it->second);
        w.pop_back();
      }
    }
  }
  std::set<Configuration> dist; // Distribution
};

#endif // DISTRIBUTION_DISCRETE_HH
