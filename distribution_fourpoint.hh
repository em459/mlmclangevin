/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the MLMCLangevin code.
 *  
 *  (c) Copyright Eike Mueller, Grigoris Katsiolides and Tony Shardlow, University of Bath
 *  
 *  Main Developer: Eike Mueller
 *  
 *  Contributors:
 *    Grigoris Katsiolides
 *    Tony Shardlow
 *  
 *  MLMCLangevin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MLMCLangevin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with MLMCLangevin (see files COPYING and COPYING.LESSER). If not,
 *  see <http://www.gnu.org/licenses/>.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/

#ifndef DISTRIBUTION_FOURPOINT_HH
#define DISTRIBUTION_FOURPOINT_HH DISTRIBUTION_FOURPOINT_HH

#include <cstdlib>
#include <cmath>
#include "distribution_discrete.hh"

/* ************************************* *
 * Class for four point distribution

 * P(xi = \pm v1) = p/2
 * P(xi = \pm v2) = (1-p)/2
 * v1=sqrt(3-sqrt(6));  v2=(3+sqrt(6))
 * p = (3+sqrt(6))/6
 * ************************************* */
class DistributionFourPoint : public DistributionDiscrete {
public:
  typedef DistributionDiscrete Base;
  DistributionFourPoint(const ParametersDistribution& parameters_dist_) :
    Base(parameters_dist_), sqrt6(sqrt(6.0)),
    p((3+sqrt6)/6),//
    v1(sqrt(3-sqrt6)), v2(sqrt(3+sqrt6)),
    c1((1-p)/2.), c2(1./2.), c3((1.+p)/2.),
    uniform_dist(0.0,1.0)
  {
    dist.insert(std::make_pair(v2, c1));
    dist.insert(std::make_pair(-v2, c1));
    dist.insert(std::make_pair(v1, c2-c1));
    dist.insert(std::make_pair(-v1, c2-c1));
    
  }

  inline void draw(const int n, double* v) const {
    for (int i=0;i<n;++i) {
      double u = uniform_dist(engine);
      if (u<c1)
        v[i]= -v2;
      else if (u<c2)
        v[i]= -v1;
      else if (u<c3)
        v[i]= v1;
      else
        v[i]= v2;
    }
  }
  
private:
  const double sqrt6, p,v1, v2, c1, c2, c3;
  typedef std::uniform_real_distribution<double> Uniform;
  mutable Uniform uniform_dist;
};

#endif // DISTRIBUTION_FOURPOINT_HH
