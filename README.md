MLMCLangevin code
=================

This repository contains a set of C++ classes for integrating the Langevin equation using Monte Carlo integrators with different time stepping methods. Both Standard Monte Carlo and more advanced Multilevel Monte Carlo methods are supported. The modular design of the code make it is easy to change, for example, the time stepping method or the equation which is studied to carry out numerical experiments.

![Random Walk](randomwalk.png)
*Fig. 1: Random walks as they can be simulated with the code*

Structure
---------
Throughout the code C++ templates are used to allow the flexible choice of algorithms while maintaining performance.

The main classes are `MonteCarloStandard` and `MonteCarloMultilevel`, which implement the general integrator algorithms for the SDE of Langevin type. The used probability distribution, the time stepping method and the quantity of interest are passed to the algorithm classes as template parameters. The time stepping classes (so far Euler-Maruyama, and both Geometric Langevin and Stoermer-Verlet splitting methods have been implemented) provide functionality for carrying out the individual time steps. They in turn take a class derived from Langevin as a template parameter. The Langevin object contains details on the actual SDE which is integrated.

So far both a Gaussian probability distribution and a three-point distribution have been implemented. Various quantities of interest are defined in the file `quantityofinterest.hh`.

Note that the different parameters of the algorithm (such as Lanegin equation, time stepping method, quantity of interest) are set at compile time to allow the compiler to optimise the code and to avoid virtual function calls. This is done in the main program `driver.cc`. However, to make the code more usable, these options can be set in a central place in the file `config.h`.

Requirements
------------
To compile the code a standard C++ compiler is required. A version compatible with the C++-11 standard is recommended to use the high resolution timers from the chrono library. The code has been tested with version 4.7.3 of the GPU C++ compiler.

Compilation
-----------
Edit the Makefile according to your system settings. Usually not many changes should be necessary since the requirements on the system are minimal.

Select the following compile time options

* Langevin equation to integrate
* Time stepping method
* Quantity of interest
* Probability distribution
* Initial distribution (currently only INITIALDISTRIBUTION_FIXED is supported)

by editing the file `config.h`.

Then simply run `make all` to build the code.

Running the code
----------------
Runtime options are read from a parameter file, see `parameters_template.in` for an example. The name of the parameter file is passed to the executable as a command line parameter:

`./driver.x <parameterfile>`

Output is print to screen and also written to two files (`results.dat` = full MLMC details, `timing.dat`=timing information on the different MLMC levels). For reference, a sample output is given below

    +-------------------+
    !   Multilevel MC   !
    +-------------------+

     ------- Parameters ------- 

    distribution parameters :
      seed = 3523525

    montecarlo parameters :
      L = 7
      Mcoarse = 1
      T = 1.000000e+00
      epsilon = 1.000000e-03

    multilevel parameters :
      Nmin = 1000
      Noversample = 1
      coarseexact = true
      ell0 = 2

    Langevin equation = Harmonic Oscillator 
    harmonicoscillator parameters :
      lambda = 1.000000e+00
      omega = 1.000000e+00
      sigma = 1.000000e+00

    Time stepping method = Geometric Langevin

    Quantity of interest = Position

    Random number distribution = Three point distribution 

    Initial State distribution = Fixed point 
    initialdistributionfixed parameters :
      U0 = -1.000000e+00
      X0 = 0.000000e+00


    Timer = std::chrono, resolution = 1.000000e-03 ms 

    ------- Results ---------- 

    *** MULTILEVEL MC ***
    Expectation value = -0.5322374848 +/- 0.0007069960

    Cost: 
      cost per timestep [fine level] = 39.68893 ns 
      cost per timestep [coarsest level] = 29.12420 ns 
      cost total = 2.17184e-02 s

    level : h, N,  Cost, Mean_{level}, Var_{level}, [Var_{level-1}/Var_{level}]
        0 : 2.50000e-01        120 3.49490e-06 s -4.63849e-01 1.76038e-01 
    --- 
        1 : 1.25000e-01      24163 7.67203e-03 s -3.35325e-02 4.26958e-03 
        2 : 6.25000e-02       8586 5.45231e-03 s -1.79972e-02 1.07802e-03  [ 3.96059 = 2^(1.98572) ]
        3 : 3.12500e-02       2998 3.80760e-03 s -9.33016e-03 2.62818e-04  [ 4.10175 = 2^(2.03624) ]
        4 : 1.56250e-02       1091 2.77124e-03 s -5.01586e-03 6.95313e-05  [ 3.77986 = 2^(1.91833) ]
        5 : 7.81250e-03        396 2.01175e-03 s -2.51296e-03 1.82923e-05  [ 3.80113 = 2^(1.92643) ]

    Elapsed time = 2.64310e-02 s

Contact
-------
All enquiries should be addressed to <e.mueller@bath.ac.uk>.

Copyright and license statement
-------------------------------

(c) Copyright Eike Mueller, Grigoris Katsiolides and Tony Shardlow,
    University of Bath
 
Main Developer: Eike Mueller
 
Contributors:
  Grigoris Katsiolides
  Tony Shardlow
  
MLMCLangevin is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

MLMCLangevin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.
  
You should have received a copy of the GNU Lesser General Public License along with MLMCLangevin (see files COPYING and COPYING.LESSER). If not, see <http://www.gnu.org/licenses/>.
