/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the MLMCLangevin code.
 *  
 *  (c) Copyright Eike Mueller, Grigoris Katsiolides and Tony Shardlow, University of Bath
 *  
 *  Main Developer: Eike Mueller
 *  
 *  Contributors:
 *    Grigoris Katsiolides
 *    Tony Shardlow
 *  
 *  MLMCLangevin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MLMCLangevin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with MLMCLangevin (see files COPYING and COPYING.LESSER). If not,
 *  see <http://www.gnu.org/licenses/>.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/

#ifndef MULTILEVELMC_HH
#define MULTILEVELMC_HH MULTILEVELMC_HH
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <algorithm>
#include <sstream>
#include <stack>
#include <set>

#include "timer.hh"
#include "montecarlo.hh"
#include "quantityofinterest.hh"
#include "estimator.hh"
#include "messages.hh"

/* ************************************* *
 * Parameters for Multilevel class
 * ************************************* */
class ParametersMultilevel : public Parameters {
public:
  // Constructor
  ParametersMultilevel(const unsigned int L) :
    Parameters("multilevel"),
    ell0_(0),
    L_(L),
    coarseexact_(false),
    Noversample_(1),
    Nmin_(1000) {
    addKey("coarseexact",Bool);
    addKey("Noversample",Integer);
    addKey("Nmin",Integer);
    addKey("ell0",Integer);
  }

  // Read from file
  int readFile(const std::string filename) {
    int readSuccess = Parameters::readFile(filename);
    if (!readSuccess) {
      coarseexact_ = contents["coarseexact"]->getBool();
      Noversample_ = contents["Noversample"]->getInt();
      Nmin_ = contents["Nmin"]->getInt();
      // Coarsest multigrid level
      ell0_ = contents["ell0"]->getInt();
      if (L_<=ell0_) {
        messagehandler.fatalError("Number of levels too low (L <= ell0).","ParametersMultilevel::Constructor()");
      }
    }
    return readSuccess;
  }

  // Return data
  bool coarseexact() const { return coarseexact_; }
  unsigned int Noversample() const { return Noversample_; }
  unsigned int NoLevels() const { return L_-ell0_+1; }
  unsigned int Nmin() const { return Nmin_; }
  unsigned int ell0() const { return ell0_; }

  // Class data
private:
  bool coarseexact_;
  unsigned int Noversample_;
  unsigned int Nmin_;
  unsigned int ell0_;
  unsigned int L_;
};

/* ************************************* *
 * State for iterative coarse level
 * construction
 * ************************************* */
struct State {
  State(const double X_,
        const double U_,
        const double S_,
        const double probability_) : X(X_), U(U_), S(S_), 
                                     probability(probability_) {}
  double X; // Position
  double U; // Velocity
  double S; // Sign of extended position variable
  double probability; // Probability of state
};

/* ************************************* *
 * Multilevel Monte Carlo integrator
 * ************************************* */
template <class Distribution,
          class Timestep,
          class QuantityOfInterest,
          class InitialDistribution>
class MonteCarloMultilevel :  public MonteCarlo<Distribution,Timestep,QuantityOfInterest,InitialDistribution> {
public:
  typedef MonteCarlo<Distribution,Timestep,QuantityOfInterest,InitialDistribution> Base;
  MonteCarloMultilevel(const Distribution &distribution_,
                       Timestep &timestep_,
                       const QuantityOfInterest &qoi_,
                       const InitialDistribution &initialdistribution_,
                       const ParametersMonteCarlo &parameters_montecarlo_,
                       const ParametersMultilevel &parameters_multilevel_,
                       const bool verbose_=false) :
    Base(distribution_,
         timestep_,
         qoi_,
         initialdistribution_,
         parameters_montecarlo_,
         verbose_),
    parameters_multilevel(parameters_multilevel_),
    Noversample(parameters_multilevel.Noversample()),
    Nlevel(parameters_multilevel.NoLevels()),
    ell0(parameters_multilevel.ell0()),
    Mcoarse(parameters_montecarlo.Mcoarse()),
    coarseexact(parameters_multilevel.coarseexact()),
    Nmin(parameters_multilevel.Nmin()),
    M(Nlevel),
    N(Nlevel),
    hfine(parameters_montecarlo.h()),
    T(parameters_montecarlo.T()),
    adaptive(parameters_montecarlo.adaptive()),
    timestep_ml(Nlevel),
    cost_ml(Nlevel) {
    double hfine = parameters_montecarlo.h();
    for (int ell=Nlevel-1;ell>=0;--ell) {
      timestep_ml[ell] = new Timestep(timestep_);
      std::stringstream label;
      label << "level " << ell;
      std::string s = "x";
      timer.push_back(Timer(s));
    }
    estimator.push_back(Estimator(qoi_.N(),coarseexact));
    for (int ell=1;ell<Nlevel;++ell) {
      estimator.push_back(Estimator(qoi_.N()));
    }
    measureCostPerTimestep();
    measureCostPerQoIEvaluation();
    distribution.reset();
  }

  /* ******************************************* *
   * Destructor. delete timesteps on all levels.
   * ******************************************* */
  ~MonteCarloMultilevel() {
    for (int ell=0;ell<Nlevel;++ell) {
      delete timestep_ml[ell];
    }
  }

  /* ******************************************* *
   * Carry out Monte Carlo integration
   * ******************************************* */
  virtual void run() {
    M[Nlevel-1] = parameters_montecarlo.M();
    double epsilon = parameters_montecarlo.epsilon();
    for (int ell=Nlevel-2;ell>=0;--ell) {
      M[ell] = M[ell+1]/2;
      if (M[ell] < 1) {
        messagehandler.fatalError("M_{fine} is not a multiple of 2^{L-1}.","MultilevelMC::run()");
      }
    }
    t_run.reset();
    t_run.start();
    bool sufficientStat = false;
    // Loop over all levels and reset estimators
    std::fill(N.begin(),N.end(),Nmin);
    for (size_t ell=0;ell<Nlevel;++ell) {
      estimator[ell].reset();
    }
    // Solve the coarsest level exactly, if requested
    if (coarseexact) {
      timer[0].start();
      coarsestLevelExact();
      timer[0].stop();
    }
    // Loop until sufficient statistics has been calculated
    do {
      // Coarsest level
      if (!coarseexact) {
        timer[0].start();
        coarsestLevel();
        timer[0].stop();
      }
      // Loop over all finer levels
      for (size_t ell=1;ell<Nlevel;++ell) {
        timer[ell].start();
        fineLevel(ell);
        timer[ell].stop();
      }
      // Readjust numbers of particles and check whether
      // sufficient statistics has been collected
      readjustN(epsilon,sufficientStat);
    } while (!sufficientStat); // Abort once we have collected sufficient stats
    // Calculate cost per level and total cost
    if (coarseexact) {
      cost_ml[0] = coarseEvaluations*costTimestepCoarsestLevel+costQoI ;
    } else {
      cost_ml[0] = N[0]*(M[0]*costTimestepCoarsestLevel+costQoI);
    }
    for (size_t ell=1;ell<Nlevel;++ell) {
      cost_ml[ell] = N[ell]*(M[ell]*costTimestepFineLevel+costQoI);
    }
    cost = 0.0;
    for (size_t ell=0;ell<Nlevel;++ell) {
      cost += cost_ml[ell];
    }
    t_run.stop();
  }

  /* ******************************************* *
   * Expectation value of quantity of interest
   * ******************************************* */
  virtual std::vector<double> E_QoI() const {
    std::vector<double> sum = estimator[0].mean();
    for (size_t ell=1;ell<Nlevel;++ell) {
      std::vector<double> mean = estimator[ell].mean();
      for (size_t i=0; i<mean.size(); ++i) {
        sum[i] += mean[i];
      }
    }
    return sum;
  }
  /* ******************************************* *
   * Return statistical error
   * ******************************************* */
  virtual std::vector<double> Error_QoI() const {
    std::vector<double> sum(estimator[0].mean().size(),0.0);
    if (!coarseexact) {
      std::vector<double> tmp = estimator[0].variance();
      for (size_t i=0; i<sum.size(); ++i) {
        sum[i] += tmp[i]/N[0];
      }
    }
    for (size_t ell=1;ell<Nlevel;++ell) {
      std::vector<double> tmp = estimator[ell].variance();
      for (size_t i=0; i<sum.size(); ++i) {
        sum[i] += tmp[i]/N[ell];
      }
    }
    for (size_t i=0; i<sum.size(); ++i) {
      sum[i] = sqrt(sum[i]);
    }
    return sum;
  }

  /* ******************************************* *
   * Return variances of quantity of interest
   * ******************************************* */
  virtual std::vector<std::vector<double> > Var_QoI() const {
    std::vector<std::vector<double> > variance(estimator[0].mean().size());
    for (int ell=0;ell<Nlevel;++ell) {
      std::vector<double> tmp=estimator[ell].variance();
      for (size_t i=0; i<variance.size(); ++i) { 
        variance[i].push_back(tmp[i]);
      }
    }
    return variance;
  }

  /* ******************************************* *
   * Return number of particles on each level
   * ******************************************* */
  virtual std::vector<unsigned int> Npart() const { return N; }

  /* ******************************************* *
   * Save output on all levels to a file
   * ******************************************* */
  void save_results(std::string filename) const {
    std::ofstream output;
    output.open(filename.c_str());
    output.precision(10);
    for (size_t i=0; i<E_QoI().size(); ++i) {
      output << " Expectation value" << "[" << i+1 << "] = " << std::fixed << E_QoI()[i]<<"\n";
    }
    output << "#   level, h, No. of particles, cost, variance" << std::endl;
    for (int ell=0;ell<Nlevel;++ell) {
      output.width(2);
      output << ell << " ";
      output.precision(5);
      output << std::scientific << hfine*pow(2.,Nlevel-1-ell);
      output.width(10);
      if ( (ell == 0) && coarseexact ) {
        output << coarseEvaluations << " ";
      } else {
        output << N[ell] << " ";
      }
      output.precision(5);
      output << std::scientific << cost_ml[ell];
      output << " ";
      output.precision(5);
      for (size_t i=0; i<estimator[ell].variance().size(); ++i) {
        output << std::scientific << estimator[ell].variance()[i];
        output << std::endl;
      }
    }
    output.close();
  }

  /* ******************************************* *
   * Save output on all levels to a file
   * ******************************************* */
  void save_matlab(std::string stem) const {
    std::ofstream output;
    std::string filenameout(stem);
    filenameout.append(".m");
    output.open(filenameout.c_str());
    output << "function [eps,E,std_E,cput,Cost,h,samples,themean,variance]="<<stem<<"\n";
    output.precision(10);
    output << " exact=  1;%(hack)\n"; // hack
    output << " eps=  " << std::fixed << parameters_montecarlo.epsilon()<<";\n";
    for (size_t i=0; i<E_QoI().size(); ++i) {
      output << " E  " << "[" << i+1 << "] = " << std::fixed << E_QoI()[i]<<";\n";
      output << " std_E  " << "[" << i+1 << "] = " << std::fixed << Error_QoI()[i]<<";\n";
    }
    output << " cput=  " << std::fixed << t_run.elapsed() <<";\n";
    output << " Cost = " << std::scientific << Base::Cost() << ";\n\n";
    output<<"ell=[";
    for (int ell=0;ell<Nlevel;++ell) {
      output.width(2);
      output << ell << " ";
    }
    output<<"];\n\n";
    output<<"h=[";
    for (int ell=0;ell<Nlevel;++ell) {
      output.precision(5);
      output << std::scientific << hfine*pow(2.,Nlevel-1-ell) <<" ";
    }
    output<<"];\n\n";
    output<<"samples=[";
    for (int ell=0;ell<Nlevel;++ell) {
      output.width(10);
      if ( (ell == 0) && coarseexact ) {
        output << coarseEvaluations << " ";
      } else {
        output << N[ell] << " ";
      }
    };
    output<<"];\n\n";
    output<<"variance=[";
    for (size_t i=0; i<E_QoI().size(); ++i) {
      for (int ell=0;ell<Nlevel;++ell) {
        output.precision(5);
        output << std::scientific << estimator[ell].variance()[i]<<" ";
      }
      output<<"\n\n";
    }
    output<<"];\n";


    output<<"themean=[";
    for (size_t i=0; i<E_QoI().size(); ++i) {
      for (int ell=0;ell<Nlevel;++ell) {
        output.precision(5);
        output << std::scientific << estimator[ell].mean()[i]<<" ";
      }
      output<<"\n\n";
    }
    output<<"];\n";

    output.close();
  }

  /* ******************************************* *
   * Save all timing results to a file
   * ******************************************* */
  void saveTimings(const std::string filename) {
    std::ofstream output;
    output.open(filename.c_str());
    for (int ell=0;ell<Nlevel;++ell) {
      output.width(3);
      output << ell;
      output << " ";
      output.precision(4);
      output << std::scientific << timer[ell].elapsed();
      output << std::endl;
    }
    output.close();
  }

  /* ******************************************* *
   * Print out results to standard output
   * ******************************************* */
  virtual void show_results() const {
    std::cout.precision(10);
    for (size_t i=0; i<E_QoI().size(); ++i) {
      std::cout << " Expectation value" << "[" << i+1 << "] = " << std::fixed << E_QoI()[i];
      std::cout << " +/- " << std::fixed << Error_QoI()[i] << std::endl;
    }
    std::cout << std::endl;
    std::cout << " Cost: " << std::endl;
    std::cout << "   cost per timestep [fine level] = ";
    std::cout.precision(5);
    std::cout << std::fixed << costTimestepFineLevel*1.E9 << " ns ";
    std::cout << std::endl;
    std::cout << "   cost per timestep [coarsest level] = ";
    std::cout.precision(5);
    std::cout << std::fixed << costTimestepCoarsestLevel*1.E9 << " ns ";
    std::cout << std::endl;
    std::cout << "   cost per quantity of interest evaluation = ";
    std::cout.precision(5);
    std::cout << std::fixed << costQoI*1.E9 << " ns ";
    std::cout << std::endl;
    std::cout.precision(5);
    std::cout << "   cost total = " << std::scientific << Base::Cost() << " s";
    std::cout << std::endl;
    std::cout << std::endl;
    for (size_t i=0; i<E_QoI().size(); ++i) {
      std::cout << "Quantity of Interest " << i+1 << std::endl; 
      std::cout << " level : h, N,  Cost, Mean_{level}, Var_{level}, [Var_{level-1}/Var_{level}]";
      std::cout << std::endl;
      for (size_t ell=0;ell<Nlevel;++ell) {
        std::cout.precision(5);
        std::cout.width(2);
        std::cout << "    " << ell << " : ";
        std::cout << std::scientific << hfine*pow(2.,Nlevel-1-ell) << " ";
        std::cout.width(10);
        if ( (ell == 0) && coarseexact ) {
          std::cout << coarseEvaluations << " ";
        } else {
          std::cout << N[ell] << " ";
        }
        std::cout.precision(5);
        std::cout << cost_ml[ell] << " s ";
        std::cout << estimator[ell].mean()[i] <<" ";
        std::cout << estimator[ell].variance()[i] << " ";
        if (ell > 1) {
          std::cout.precision(5);
          double varRatio = estimator[ell-1].variance()[i]/estimator[ell].variance()[i];
          std::cout << " [ " << std::fixed << varRatio;
          std::cout << " = 2^(" << std::fixed << log(varRatio)/log(2.);
          std::cout << ") ]";
        }
        if (ell == 0) std::cout << std::endl << "   --- ";
        std::cout << std::endl;
      }
      std::cout << std::endl << std::endl;
    }
    std::cout << std::endl;
    std::cout << " Elapsed time = " << std::scientific << t_run.elapsed() << " s" << std::endl;
    std::cout << std::endl;
  }

  /* ******************************************* *
   * Return cost of one time step on fine level
   * ******************************************* */
  double costPerTimestepFineLevel() const {
    return costTimestepFineLevel;
  }

  /* ******************************************* *
   * Return cost of one time step on coarsest level
   * ******************************************* */
  double costPerTimestepCoarsestLevel() const {
    return costTimestepCoarsestLevel;
  }

  /* ******************************************* *
   * Return cost of evaluating and
   * accumulating the quantity of interest
   * ******************************************* */
  double costPerQoIEvaluation() const {
    return costQoI;
  }

private:
  /* ******************************************* *
   * Measure cost of time steps
   * ******************************************* */
  void measureCostPerTimestep() {
    unsigned int nSteps = (1 << 10); // Calculate 2^10 = 1024
    int ell=Nlevel-1;
    double U_coarse;
    double X_coarse;
    initialdistribution.draw(U_coarse,X_coarse);
    double U_fine;
    double X_fine;
    double t_total = 0.0;
    while (t_total < 1.0) {
      nSteps *= 2;
      // Fine level
      initialdistribution.draw(U_coarse,X_coarse);
      U_fine = U_coarse;
      X_fine = X_coarse;
      Timer t_fineLevel;
      t_fineLevel.start();
      double two_hfine = 2.0*hfine;
      double S_fine = 1.0;
      double S_coarse = 1.0;
      for (unsigned int i=0;i<nSteps/2;++i) {
        double xi_fine[2*Timestep::nxi];
        double xi_coarse[Timestep::nxi];
        distribution.draw(2*Timestep::nxi,xi_fine);
        // Fine level step
        timestep_ml[ell]->step(&xi_fine[0],hfine,U_fine,X_fine,
                               S_fine);
        timestep_ml[ell]->step(&xi_fine[Timestep::nxi],hfine,U_fine,X_fine,
                               S_fine);
        timestep_ml[ell]->CombineIncrements(xi_fine,xi_coarse,hfine,X_coarse);
        // Coarse level step
        timestep_ml[ell-1]->step(xi_coarse,two_hfine,
                                 U_coarse,X_coarse,S_coarse);
      }
      t_fineLevel.stop();
      t_total = t_fineLevel.elapsed();
    }
    costTimestepFineLevel = t_total/double(nSteps);
    t_total = 0.0;
    nSteps = (1 << 10);
    while (t_total < 1.0) {
      nSteps *= 2;
      // Coarsest level
      Timer t_coarsestLevel;
      t_coarsestLevel.start();
      double S_coarsest = 1.0;
      for (unsigned int i=0;i<nSteps;++i) {
        double xi[Timestep::nxi];
        distribution.draw(Timestep::nxi,xi);
        timestep_ml[0]->step(xi,hfine,U_coarse,X_coarse,S_coarsest);
      }
      t_coarsestLevel.stop();
      t_total = t_coarsestLevel.elapsed();
    }
    costTimestepCoarsestLevel = t_total/double(nSteps);
    // DO NOT REMOVE THIS LINE: It ensure that the code is actually compiled
    // and executed to get the timings
    X_tmp = X_coarse+X_fine;
  }

  /* ******************************************* *
   * Measure cost of evaluating and 
   * accumulating the quantity of interest
   * ******************************************* */
  void measureCostPerQoIEvaluation() {
    unsigned int nSamples = (1 << 10); // Calculate 2^10 = 1024
    double U;
    double X;
    double t_total = 0.0;
    while (t_total < 1.0) { 
      nSamples *= 2;
      initialdistribution.draw(U,X);
      Timer t_step;
      t_step.start();
      for (unsigned int i=0;i<nSamples;++i) {
        estimator[0].accumulate(qoi.evaluate(X,U));
      }
      t_step.stop();
      t_total = t_step.elapsed();
    }
    costQoI = t_total/double(nSamples);
  }

  /* ******************************************* *
   * Exact integration on coarsest level
   * Note that this only works for a discrete
   * distribution and for fixed (U_0,X_0)
   * ******************************************* */
  void coarsestLevelExact() {
    typedef typename Distribution::ProductConfiguration ProductConfiguration;
    typename std::set<ProductConfiguration> p;
    unsigned int nSteps = (Mcoarse << ell0);
    double hcoarse_uniform = hfine*pow(2.,Nlevel-1);
    // Create product distribution for nxi configurations, where
    // nxi is the number of random variables per time step
    try {
      p = distribution.productDistribution(Timestep::nxi);
    } catch (ProductDistributionNotImplementedException e) {
      messagehandler.fatalError(e.what(),"MultilevelMC::coarsestLevelExact()");
    }
    // Create vectors with states and probabilities
    double* xi = new double[Timestep::nxi*p.size()];
    double* prob = new double[p.size()];
    int i=0;
    for (typename std::set<ProductConfiguration>::const_iterator it=p.begin();
         it!=p.end();++it) {
      std::copy(it->first.begin(),it->first.end(),&xi[Timestep::nxi*i]);
      prob[i] = it->second;
      i++;
    }
    // Iteratively traverse probability tree
    std::stack<int> branch;
    std::stack<State> state;
    branch.push(0);
    double U;
    double X;
    initialdistribution.draw(U,X);
    state.push(State(X,U,1.0,1.0));
    coarseEvaluations = 0;
    while (!branch.empty()) {
      // If we are at the final timestep, evaluate and step back
      State curState = state.top();
      U = curState.U;
      X = curState.X;
      double S_coarsest = curState.S;
      double probability = curState.probability;
      if (branch.size() == nSteps+1) {
        estimator[0].accumulate(qoi.evaluate(X,U),probability);
        branch.pop();
        state.pop();
      } else {
        int curBranch = branch.top();
        if (curBranch == p.size()) {
          // Reached last branch, step back
          branch.pop();
          state.pop();
        } else {
          // Carry out time step, add next level and decrease branch
          // pointer of current stack level
          timestep_ml[0]->step(&xi[curBranch*Timestep::nxi],hcoarse_uniform,U,X,S_coarsest);
          coarseEvaluations++;
          branch.pop();
          branch.push(curBranch+1);
          state.push(State(X,U,S_coarsest,probability*prob[curBranch]));
          branch.push(0);
        }
      }
    }
    delete[] xi;
    delete[] prob;
  }

  /* ******************************************* *
   * MC integration on coarsest level
   * ******************************************* */
  void coarsestLevel() {
    // Loop over particles
    unsigned int N_start = estimator[0].samples();
    unsigned int N_finish = std::max(Nmin,Noversample*N[0]);
    double time = T - 1.e-12; // makes sure that the while loop will not exceed T due to rounding errors
    double hcoarse_uniform = hfine*pow(2.,Nlevel-1);
    double hcoarse_adaptive = hcoarse_uniform; 
    for (unsigned int i=N_start;i<N_finish;++i) {
      double S_coarsest = 1.0;
      // Loop over timesteps
      double U;
      double X;
      initialdistribution.draw(U,X);
      double t = 0.0;
      while (t < time) {
        double xi[Timestep::nxi];
        distribution.draw(Timestep::nxi,xi);
        if (adaptive) {
          hcoarse_adaptive = timestep_ml[0]->adapt(hcoarse_uniform,X);
          hcoarse_adaptive = std::min(hcoarse_adaptive,T-t);
        }
        timestep_ml[0]->step(xi,hcoarse_adaptive,U,X,S_coarsest);
        t+=hcoarse_adaptive;
      }
      estimator[0].accumulate(qoi.evaluate(X,U));
    }
  }

  /* ******************************************* *
   * MC integration on finer levels
   * ******************************************* */
  void fineLevel(const unsigned int ell) {
    // Loop over particles
    unsigned int N_start = estimator[ell].samples();
    unsigned int N_finish = std::max(Noversample*N[ell],Nmin);
    double time = T - 1.e-12;
    double hfine_uniform = hfine*pow(2.,Nlevel-1-ell);
    double two_hfine_uniform = 2.0*hfine_uniform;
    for (unsigned int i=N_start;i<N_finish;++i) {
      // Loop over timesteps
      double U_coarse;
      double X_coarse;
      initialdistribution.draw(U_coarse,X_coarse);
      double U_fine = U_coarse;
      double X_fine = X_coarse;
      double S_fine = 1.0;
      double S_coarse = 1.0;
      double t = 0.0;
      if (adaptive) {
        double t_coarse=0.0, t_fine=0.0;
        double h_coarse=0.0, h_fine=0.0;
        double DW_coarse[Timestep::nxi], DW_fine[Timestep::nxi];
        DW_coarse[0]=0.0, DW_fine[0]=0.0;
        double z=1.e-12;
        while (t < time) {
          double t_old=t;
          t = std::min(t_coarse, t_fine);
          double DW[Timestep::nxi];
          distribution.draw(Timestep::nxi,DW);
          double time_diff = t-t_old;
          timestep_ml[ell]->CombineIncrementsAdaptive(DW,DW_coarse,DW_fine,X_coarse,X_fine,time_diff);
          if (std::abs(t-t_coarse) < z) {
            timestep_ml[ell-1]->AdaptiveStep(DW_coarse,h_coarse,
                                             U_coarse,X_coarse,S_coarse);      
            h_coarse = timestep_ml[ell-1]->adapt(two_hfine_uniform,X_coarse);
            h_coarse = std::min(h_coarse,T-t_coarse);
            t_coarse+=h_coarse;
            DW_coarse[0]=0.0;
          }
          if (std::abs(t-t_fine) < z) {
            timestep_ml[ell]->AdaptiveStep(&DW_fine[0],h_fine,U_fine,X_fine,
                                           S_fine);
            h_fine = timestep_ml[ell]->adapt(hfine_uniform,X_fine);
            h_fine = std::min(h_fine,T-t_fine);
            t_fine+=h_fine;
            DW_fine[0]=0.0;
          }
        }
      } else {
        while (t < time) {
          double xi_fine[2*Timestep::nxi];
          double xi_coarse[Timestep::nxi];
          distribution.draw(2*Timestep::nxi,xi_fine);
          // Fine level step
          timestep_ml[ell]->step(&xi_fine[0],hfine_uniform,U_fine,X_fine,
                                 S_fine);
          timestep_ml[ell]->step(&xi_fine[Timestep::nxi],hfine_uniform,U_fine,X_fine,
                                 S_fine);
          timestep_ml[ell]->CombineIncrements(xi_fine,xi_coarse,hfine_uniform,X_coarse);
          // Coarse level step
          timestep_ml[ell-1]->step(xi_coarse,two_hfine_uniform,
                                   U_coarse,X_coarse,S_coarse);
          t+=two_hfine_uniform;
        }
      }
      std::vector<double> diff = qoi.evaluate(X_fine,U_fine);
      std::vector<double> tmp = qoi.evaluate(X_coarse,U_coarse);
      for (size_t i=0; i<diff.size(); ++i) {
        diff[i] -= tmp[i];
      }
      estimator[ell].accumulate(diff);
    }
  }

  /* ******************************************* *
   * Calculate new N according to Mike Giles' formula (12)
   * ******************************************* */
  void readjustN(const double epsilon, bool& sufficientStat) {
    double sum_Voverh = 0.0;
    std::vector<double> Vh(Nlevel);
    int ellMin = 0;
    // Ignore the coarsest level if we solve exactly there
    if (coarseexact) ellMin = 1;
    for (size_t ell=ellMin;ell<Nlevel;++ell) {
      double h = hfine*pow(2.,Nlevel-1-ell);
      Vh[ell] = sqrt(estimator[ell].max_variance()*h);
      sum_Voverh += sqrt(estimator[ell].max_variance()/(h));
    }
    sufficientStat = true;
    for (size_t ell=ellMin;ell<Nlevel;++ell) {
      N[ell] = (unsigned int) ceil(2./(epsilon*epsilon)*Vh[ell]*sum_Voverh);
      // Check if N_calc[ell] >= N[ell]
      sufficientStat = sufficientStat
        && (estimator[ell].samples() >= std::max(Noversample*N[ell],Nmin) );
    }
    if (verbose) {
      std::cout << "   ...adjusting N to ";
      for (size_t ell=0;ell<Nlevel;++ell) {
        if ( (ell == 0) && (coarseexact) ) {
          std::cout << "exact" << " [" << ell << " ] ";
        } else {
          std::cout << N[ell] << " [" << ell << " ] ";
        }
      }
      std::cout << std::endl;
    }
  }

private:
  using Base::distribution;
  using Base::timestep;
  using Base::qoi;
  using Base::initialdistribution;
  using Base::parameters_montecarlo;
  using Base::verbose;
  using Base::cost;
  unsigned int coarseEvaluations;   // Number of time steps on coarsest level
  double costTimestepCoarsestLevel; // } Cost per time step on different levels
  double costTimestepFineLevel;     // }
  double costQoI;
  using Base::messagehandler;
  using Base::t_run;
  const ParametersMultilevel &parameters_multilevel;
  unsigned int Nlevel;               // Number of levels
  unsigned int Mcoarse;              // }  coarsest level has
  unsigned int ell0;                 // }  Mcoarse*2^ell0 timesteps
  bool coarseexact;                  // Use exact coarse level estimator
  std::vector<Timestep*> timestep_ml; // Array of time steps on each level
  std::vector<unsigned int> N;       // Number of samples on each level
  std::vector<Estimator> estimator;  // Estimator on all levels
  std::vector<double> M;             // Number of timesteps on a level
  std::vector<double> cost_ml;       // Cost on each level
  double hfine;                      // Finest timestep size
  double T;                          // Time
  bool adaptive;                     // Adaptive timesteps?

  unsigned int Nmin;                // Minimum number of samples
  unsigned int Noversample;         // Oversampling factor
  std::vector<Timer> timer;
  double X_tmp;
};
#endif // MULTILEVELMC_HH
