/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the MLMCLangevin code.
 *  
 *  (c) Copyright Eike Mueller, Grigoris Katsiolides and Tony Shardlow, University of Bath
 *  
 *  Main Developer: Eike Mueller
 *  
 *  Contributors:
 *    Grigoris Katsiolides
 *    Tony Shardlow
 *  
 *  MLMCLangevin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MLMCLangevin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with MLMCLangevin (see files COPYING and COPYING.LESSER). If not,
 *  see <http://www.gnu.org/licenses/>.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/

#ifndef LANGEVIN_QO_HH
#define LANGEVIN_QO_HH LANGEVIN_QO_HH

#include "langevin.hh"
#include "distribution.hh"
#include "parameters.hh"

/* ************************************* *
 * Parameters for the quartic oscillator
 * ************************************* */
class ParametersQO : public Parameters {
public:
  // Constructor
  ParametersQO() :
    Parameters("quarticoscillator"),
    omega_(1.0),
    x0_(1.0),
    lambda_(1.0),
    sigma_(1.0) {
    addKey("omega",Double);
    addKey("x0",Double);
    addKey("lambda",Double);
    addKey("sigma",Double);
  }

  // Read from file
  int readFile(const std::string filename) {
    int readSuccess = Parameters::readFile(filename);
    if (!readSuccess) {
      omega_ = contents["omega"]->getDouble();
      x0_ = contents["x0"]->getDouble();
      lambda_ = contents["lambda"]->getDouble();
      sigma_ = contents["sigma"]->getDouble();
    }
    return readSuccess;
  }

  // Return data
  double omega() const { return omega_; }
  double x0() const { return x0_; }
  double lambda() const { return lambda_; }
  double sigma() const { return sigma_; }

  // Class data
private:
  double omega_;
  double x0_;
  double lambda_;
  double sigma_;
};

/* **************************************** *
   Class representing a langevin process for
   the Quartic ocillator (double well) potential:

   V(X) = 1/8*omega^2/x_0^2*(x^2-x_0^2)^2
   lambda(X) = lambda_0 = const.
   sigma(X) = sigma_0 = const.

   Note that V"(x_0) = omega^2, so that omega
   is the angular frequency at the potential
   minimum x=x_0.
   * **************************************** */
class LangevinQO : public LangevinBase {
public:
  typedef LangevinBase Base;
  // Constructor
  LangevinQO(const ParametersQO& parameters_qo_,
             const ParametersDistribution& parameters_dist_) :
    Base(parameters_dist_),
    parameters_qo(parameters_qo_),
    lambda0(parameters_qo.lambda()),
    sigma0(parameters_qo.sigma()),
    x0(parameters_qo.x0()),
    x0sq(x0*x0),
    omega0(parameters_qo.omega()),
    CV(0.125*omega0*omega0/x0sq),
    CgradV(0.5*omega0*omega0/x0sq) {}

  // lambda
  inline double lambda(const double X) const {
    return lambda0;
  }
  // sigma
  inline double sigma(const double X) const {
    return sigma0;
  }
  // Potential
  inline double V(const double X, const double U) const {
    double tmp = X*X-x0sq;
    return CV*tmp*tmp;
  }
  // Gradient of potential
  inline double gradV(const double X, const double U) const {
    return CgradV*X*(X*X-x0sq);
  }

protected:
  const ParametersQO &parameters_qo;
  const double lambda0;
  const double sigma0;
  const double omega0;
  const double x0;
  const double x0sq;
  const double CV;
  const double CgradV;
};

#endif // LANGEVIN_QO_HH
