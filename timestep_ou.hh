/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the MLMCLangevin code.
 *  
 *  (c) Copyright Eike Mueller, Grigoris Katsiolides and Tony Shardlow, University of Bath
 *  
 *  Main Developer: Eike Mueller
 *  
 *  Contributors:
 *    Grigoris Katsiolides
 *    Tony Shardlow
 *  
 *  MLMCLangevin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MLMCLangevin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with MLMCLangevin (see files COPYING and COPYING.LESSER). If not,
 *  see <http://www.gnu.org/licenses/>.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/

#ifndef TIMESTEP_OU_HH
#define TIMESTEP_OU_HH TIMESTEP_OU_HH
#include <cmath>
#include <cassert>
#include "timestep_splitting.hh"

/* ************************************* *
 * Class for one time step using
 * (1) the OU process for velocity
 * (2) symplectic Euler for position
 * ************************************* */
template <class L>
class TimestepOU : public TimestepSplitting<L,1> {
public:
  typedef TimestepSplitting<L,1> Base;
  /* Constructor */
  TimestepOU<L>(const L& langevin_) :
  Base(langevin_) {}

  /* One time step: exact OU/symplectic Euler */
  inline void step(const double *xi,
                   double &hi,
                   double &U,
                   double &X,
                   double &S) const {
    double lambda = langevin.lambda(X);
    double mu1 = exp(-hi*lambda);
    double mu2 = (1.0 - mu1)/lambda;
    // OU step
    U = mu1*U - mu2*langevin.gradV(X,U) + langevin.sigma(X)*sqrt(mu2*(0.5 + 0.5*mu1))*S*xi[0];
    // Symplectic Euler step
    X += U*hi;
    langevin.applyBoundaryConditions(X,U,S);
  }
  using Base::langevin;
  using Base::OUstep;
};

#endif // TIMESTEP_OU_HH
