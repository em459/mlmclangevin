/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the MLMCLangevin code.
 *  
 *  (c) Copyright Eike Mueller, Grigoris Katsiolides and Tony Shardlow, University of Bath
 *  
 *  Main Developer: Eike Mueller
 *  
 *  Contributors:
 *    Grigoris Katsiolides
 *    Tony Shardlow
 *  
 *  MLMCLangevin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MLMCLangevin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with MLMCLangevin (see files COPYING and COPYING.LESSER). If not,
 *  see <http://www.gnu.org/licenses/>.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/

#ifndef DISTRIBUTION_THREEPOINT_HH
#define DISTRIBUTION_THREEPOINT_HH DISTRIBUTION_THREEPOINT_HH

#include <cstdlib>
#include <cmath>
#include "distribution_discrete.hh"

/* ************************************* *
 * Class for three point distribution
 * P(xi = 0) = 2/3
 * P(xi = \sqrt{3}) = 1/6
 * P(xi = -\sqrt{3}) = 1/6
 * ************************************* */
class DistributionThreePoint : public DistributionDiscrete {
public:
  typedef DistributionDiscrete Base;
  DistributionThreePoint(const ParametersDistribution& parameters_dist_) :
    Base(parameters_dist_), sqrt3(sqrt(3.0)), uniform_dist(0,5) {
    dist.insert(std::make_pair(0.0,2./3.));
    dist.insert(std::make_pair(sqrt3,1./6));
    dist.insert(std::make_pair(-sqrt3,1./6));
  }

  inline void draw(const int n, double* v) const {
    for (int i=0;i<n;++i) {
      switch(uniform_dist(engine)) {
      case 0 :
        v[i] = sqrt3;
        break;
      case 1 :
        v[i] = -sqrt3;
        break;
      default :
        v[i] = 0.0;
        break;
      }
    }
  }
private:
  const double sqrt3;
  typedef std::uniform_int_distribution<> Uniform;
  mutable Uniform uniform_dist;
};

#endif // DISTRIBUTION_THREEPOINT_HH
