/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the MLMCLangevin code.
 *  
 *  (c) Copyright Eike Mueller, Grigoris Katsiolides and Tony Shardlow, University of Bath
 *  
 *  Main Developer: Eike Mueller
 *  
 *  Contributors:
 *    Grigoris Katsiolides
 *    Tony Shardlow
 *  
 *  MLMCLangevin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MLMCLangevin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with MLMCLangevin (see files COPYING and COPYING.LESSER). If not,
 *  see <http://www.gnu.org/licenses/>.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/

#ifndef DISTRIBUTION_NORMAL_HH
#define DISTRIBUTION_NORMAL_HH DISTRIBUTION_NORMAL_HH

#include <cstdlib>
#include <cmath>
#include "distribution.hh"

#include <random>
/* ************************************* *
 * Class for normal distribution N(0,1)
 * using the GNU random number generator
 * ************************************* */
class DistributionNormal : public Distribution {
public:
  typedef Distribution Base;
  DistributionNormal(const ParametersDistribution& parameters_dist_) :
    Base(parameters_dist_) {}

  inline void draw(const int n, double* v) const {
    for (int i=0;i<n;++i) {
      v[i] = normal_dist(engine);
    }
  }

private:
  typedef std::normal_distribution<double> Normal; // Distribution
  mutable Normal normal_dist;
};

#endif // DISTRIBUTION_NORMAL_HH
