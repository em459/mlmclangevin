/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the MLMCLangevin code.
 *  
 *  (c) Copyright Eike Mueller, Grigoris Katsiolides and Tony Shardlow, University of Bath
 *  
 *  Main Developer: Eike Mueller
 *  
 *  Contributors:
 *    Grigoris Katsiolides
 *    Tony Shardlow
 *  
 *  MLMCLangevin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MLMCLangevin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with MLMCLangevin (see files COPYING and COPYING.LESSER). If not,
 *  see <http://www.gnu.org/licenses/>.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/

#ifndef DISTRIBUTION_HH
#define DISTRIBUTION_HH DISTRIBUTION_HH

#include <cstdlib>
#include <vector>
#include <utility>
#include <set>
#include <exception>
#include <string>
#include <random>
#include <chrono>
#include "parameters.hh"

/* ************************************* *
 * Parameters for distribution
 * ************************************* */
class ParametersDistribution : public Parameters {
public:
  // Constructor
  ParametersDistribution() :
    Parameters("distribution"),
    seed_(1),
    fixed_seed_(true) {
    addKey("seed",Integer);
  }

  // Read from file
  int readFile(const std::string filename) {

    int readSuccess = Parameters::readFile(filename);
    if (!readSuccess) {
      int user_seed = contents["seed"]->getInt();
      if (user_seed < 0) {
        seed_ = 1;
        fixed_seed_ = false;
      } else {
        seed_ = user_seed;
        fixed_seed_ = true;
      }
    }
    return readSuccess;
  }

  // Return data
  unsigned int Seed() const { return seed_; }

  // Seed with time?
  bool FixedSeed() const { return fixed_seed_; }

  // Class data
private:
  unsigned int seed_;
  bool fixed_seed_;
};
/* ************************************* *
 * Exception
 * ************************************* */
class ProductDistributionNotImplementedException : public std::exception {
public:
  virtual const char* what() const throw()
  {
    return "method productDistribution() not implemented for this distribution.";
  }
};

/* ************************************* *
 * Base class for probability distribution
 * ************************************* */
class Distribution {
public:
  typedef std::pair<double,double> Configuration;
  typedef std::pair<std::vector<double>,double> ProductConfiguration;
  Distribution(const ParametersDistribution& parameters_dist_) :
    parameters_dist(parameters_dist_) {
    reset();
  }

  // Draw random number
  inline void draw(const int n, double* v) const {}

  // Re-seed distribution
  void reset() const {
    unsigned long int seed;
    if (parameters_dist.FixedSeed()) {
      seed = parameters_dist.Seed();
    } else {
      typedef std::chrono::system_clock Clock;
      typedef std::chrono::milliseconds Milliseconds;
      Clock::time_point now = Clock::now();
      Clock::duration time_since_1970 = now.time_since_epoch();
      seed = std::chrono::duration_cast<Milliseconds>(time_since_1970).count() % (1 << 30);
    }
    std::cout << "  Initialising random number generator with seed " << seed << std::endl;
    std::cout << std::endl;
    engine.seed(seed);
  }

  // Calculate the product distribution of n
  std::set<ProductConfiguration> productDistribution(const int n) const {
    throw ProductDistributionNotImplementedException();
  }
protected:
  const ParametersDistribution& parameters_dist;
  typedef std::mt19937_64 Engine; // Engine
  mutable Engine engine;
};


#endif // DISTRIBUTION_HH
