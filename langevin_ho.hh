/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the MLMCLangevin code.
 *  
 *  (c) Copyright Eike Mueller, Grigoris Katsiolides and Tony Shardlow, University of Bath
 *  
 *  Main Developer: Eike Mueller
 *  
 *  Contributors:
 *    Grigoris Katsiolides
 *    Tony Shardlow
 *  
 *  MLMCLangevin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MLMCLangevin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with MLMCLangevin (see files COPYING and COPYING.LESSER). If not,
 *  see <http://www.gnu.org/licenses/>.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/

#ifndef LANGEVIN_HO_HH
#define LANGEVIN_HO_HH LANGEVIN_HO_HH

#include "langevin.hh"
#include "distribution.hh"
#include "parameters.hh"

/* ************************************* *
 * Parameters for Harmonic oscillator
 * ************************************* */
class ParametersHO : public Parameters {
public:
  // Constructor
  ParametersHO() :
    Parameters("harmonicoscillator"),
    omega_(1.0),
    lambda_(1.0),
    sigma_(1.0) {
    addKey("omega",Double);
    addKey("lambda",Double);
    addKey("sigma",Double);
  }

  // Read from file
  int readFile(const std::string filename) {
    int readSuccess = Parameters::readFile(filename);
    if (!readSuccess) {
      omega_ = contents["omega"]->getDouble();
      lambda_ = contents["lambda"]->getDouble();
      sigma_ = contents["sigma"]->getDouble();
    }
    return readSuccess;
  }

  // Return data
  double omega() const { return omega_; }
  double lambda() const { return lambda_; }
  double sigma() const { return sigma_; }

  // Class data
private:
  double omega_;
  double lambda_;
  double sigma_;
};

/* **************************************** *
   Class representing a langevin process for
   the harmonic oscillator:

   V(X) = 1/2*omega_0^2*X^2
   lambda(X) = lambda_0 = const.
   sigma(X) = sigma_0 = const.

   * **************************************** */
class LangevinHO : public LangevinBase {
public:
  typedef LangevinBase Base;
  // Constructor
  LangevinHO(const ParametersHO& parameters_ho_,
             const ParametersDistribution& parameters_dist_) :
    Base(parameters_dist_),
    parameters_ho(parameters_ho_),
    lambda0(parameters_ho.lambda()),
    sigma0(parameters_ho.sigma()),
    omega0(parameters_ho.omega()),
    CV(0.5*omega0*omega0),
    CgradV(omega0*omega0),
    sqrtkBT(sigma0/(2.*sqrt(lambda0)))
  { twopi = 8.*atan(1.0); }

  // lambda
  inline double lambda(const double X) const {
    return lambda0;
  }
  // sigma
  inline double sigma(const double X) const {
    return sigma0;
  }
  // Potential
  inline double V(const double X, const double U) const {
    return CV*X*X;
  }
  // Gradient of potential
  inline double gradV(const double X, const double U) const {
    return CgradV*X;
  }

  // Sample from Gibbs distribution
  void draw(double &U, double &X) const {
    double u1 = 1.0*(rand()+1.0)/(RAND_MAX+1.);
    double u2 = 1.0*(rand()+1.0)/(RAND_MAX+1.);
    double s = sqrt(-2.*log(u1));
    U = sqrtkBT * s*sin(twopi*u2);
    X = sqrtkBT/omega0 * s*cos(twopi*u2);
  }

protected:
  const ParametersHO &parameters_ho;
  const double lambda0;
  const double sigma0;
  const double omega0;
  const double CV;
  const double CgradV;
  const double sqrtkBT;
  double twopi;
};

#endif // LANGEVIN_HO_HH
