/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the MLMCLangevin code.
 *  
 *  (c) Copyright Eike Mueller, Grigoris Katsiolides and Tony Shardlow, University of Bath
 *  
 *  Main Developer: Eike Mueller
 *  
 *  Contributors:
 *    Grigoris Katsiolides
 *    Tony Shardlow
 *  
 *  MLMCLangevin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MLMCLangevin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with MLMCLangevin (see files COPYING and COPYING.LESSER). If not,
 *  see <http://www.gnu.org/licenses/>.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/

#ifndef DISTRIBUTION_TWOPOINT_HH
#define DISTRIBUTION_TWOPOINT_HH DISTRIBUTION_TWOPOINT_HH

#include <cstdlib>
#include "distribution_discrete.hh"

/* ************************************* *
 * Class for bimodal distribution
 * P(\xi = 1) = 1/2, P(\xi = -1) = -1/2
 * ************************************* */
class DistributionTwoPoint : public DistributionDiscrete {
public:
  typedef DistributionDiscrete Base;
  DistributionTwoPoint(const ParametersDistribution& parameters_dist_) :
    Base(parameters_dist_), uniform_dist(0,1) {
    dist.insert(std::make_pair(1.0,0.5));
    dist.insert(std::make_pair(-1.0,0.5));
  }

  inline void draw(const int n, double* v) const {
    for (int i=0;i<n;++i) {
      if (uniform_dist(engine) & 1) {
        v[i] = +1.0;
      } else {
        v[i] = -1.0;
      }
    }
  }
  typedef std::uniform_int_distribution<> Uniform;
  mutable Uniform uniform_dist;
};

#endif // DISTRIBUTION_TWOPOINT_HH
