/*** COPYRIGHT AND LICENSE STATEMENT *** 
 *
 *  This file is part of the MLMCLangevin code.
 *  
 *  (c) Copyright Eike Mueller, Grigoris Katsiolides and Tony Shardlow, University of Bath
 *  
 *  Main Developer: Eike Mueller
 *  
 *  Contributors:
 *    Grigoris Katsiolides
 *    Tony Shardlow
 *  
 *  MLMCLangevin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MLMCLangevin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with MLMCLangevin (see files COPYING and COPYING.LESSER). If not,
 *  see <http://www.gnu.org/licenses/>.
 *
 *** COPYRIGHT AND LICENSE STATEMENT ***/

#include "timer.hh"

// Start timer
void Timer::start() {
#ifdef USE_CHRONO
  tp_start = Clock::now();
#else
  tp_start = clock();
#endif // USE_CHRONO
}

// Stop timer
void Timer::stop() {
#ifdef USE_CHRONO
  tp_finish = Clock::now();
  std::chrono::duration<double> t_duration = tp_finish - tp_start;
  t_elapsed = double(t_duration.count());
#else
  tp_finish = clock();
  t_elapsed += double(tp_finish-tp_start)/double(CLOCKS_PER_SEC);
#endif // USE_CHRONO
}

// Reset timer
void Timer::reset() {
  stop();
  t_elapsed = 0.0;
}

// Timer resolution
double Timer::resolution() {
#ifdef USE_CHRONO
  return double(Clock::period::num) / double(Clock::period::den);
#else
  return 0.01;
#endif // USE_CHRONOE
}

// Timer type
#ifdef USE_CHRONO
const std::string Timer::type="std::chrono";
#else
const std::string Timer::type="clock()";
#endif // USE_CHRONO

std::ostream& operator<<(std::ostream& out, Timer& t) {
  out.precision(4);
  out << " >>> [timer " << t.Label() << "] : " << std::scientific << t.elapsed();
  return out;
}
